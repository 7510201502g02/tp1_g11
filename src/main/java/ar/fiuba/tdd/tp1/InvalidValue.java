package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.exceptions.InvalidValueException;

public class InvalidValue extends Value {

    private String errorMessage;

    private void init(String rawValue) {
        this.valueType = "invalid";
        this.rawValue = rawValue;
        this.errorMessage = "invalid value";
    }

    public InvalidValue() {
        init(null);
    }

    public InvalidValue(String errorMessage) {
        init(null);
        this.errorMessage = errorMessage;
    }

    public InvalidValue(TransferPackage transferPackage) {
        init(transferPackage.value);
    }

    @Override
    public Value createFrom(TransferPackage transferPackage) {

        return new InvalidValue(transferPackage);

    }

    @Override
    public Double valueAsDouble() {

        throw new InvalidValueException(errorMessage);

    }

    @Override
    public String valueAsString() {

        return getRawValue();

    }

    @Override
    public String getRawValue() {

        return new InvalidValueException(errorMessage).getMessage();

    }

    @Override
    public boolean valid(TransferPackage transferPackage) {
        return internalValid(transferPackage);
    }

    @Override
    protected boolean internalValid(TransferPackage transfer) {
        return false;
    }
}
