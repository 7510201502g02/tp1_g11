package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormatterException;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Sheet extends Workspace {

    private List<CellInterface> cellInterfaces = new LinkedList<>();

    private CellsReferenceManager cellReferenceManager;

    public Sheet(String name) {

        this.name = name;

    }

    public Sheet() {

    }

    @Override
    public Sheet createNewWorkspace(String name) {
        return new Sheet(name);
    }

    public void setCellValue(String cellPosition, String value) {

        CellInterface requiredCell;

        try {

            requiredCell = getCell(cellPosition);

        } catch (NoSuchElementException e) {

            requiredCell = new Cell(createPosition(cellPosition), value, cellReferenceManager);
            cellInterfaces.add(requiredCell);

        }

        requiredCell.setValueSince(value);

    }

    private void setCell(CellInterface cellInterface) {

        try {

            cleanCell(cellInterface.getPosition().toString());

        } catch (Exception e) { /* do nothing */ }

        this.cellInterfaces.add(cellInterface);
    }

    // acumula formatos!
    public void setFormatTo(String cellPosition, String formatType, String format) {

        CellInterface requiredCell = getCell(cellPosition);

        Creator<CellFormatter> formatterCreator = new FormatterCreator();
        TransferPackage packageToBuildFormatter = new TransferPackage(format, formatType, requiredCell);

        CellFormatter cellFormatter = formatterCreator.createFrom(packageToBuildFormatter);

        this.validateNewFormatter(cellFormatter);

        setCell(cellFormatter);


    }

    public void cleanAllFormattersOnCell(String position) {

        String value = getCell(position).getRawValue();
        cleanCell(position);
        setCellValue(position, value);

    }

    public void setCellValueWithFormatter(String cellPosition, String value, String formatType, String format) {

        setCellValue(cellPosition, value);
        setFormatTo(cellPosition, formatType, format);

    }

    public void setValueTypeTo(String cellPosition, String valueType) {

        CellInterface requiredCell = getCell(cellPosition);
        requiredCell.setValueType(valueType);
    }

    private Position createPosition(String cellPosition) {

        Position position = new Position(cellPosition);
        // lo mas probable es que venga sin la referencia de la sheet a la que pertenece
        position.setSheetPosition(this.getName());

        return position;
    }

    public CellInterface getCell(Position position) {

        return cellInterfaces.stream()
                .filter(it -> it.getPosition().samePosition(position))
                .findFirst().get();
    }

    public CellInterface getCell(String position) {

        return getCell(createPosition(position));
    }

    public void setCellReferenceManager(CellsReferenceManager cellReferenceManager) {
        this.cellReferenceManager = cellReferenceManager;
    }

    public CellsReferenceManager getCellReferenceManager() {
        return cellReferenceManager;
    }

    // agregado para hacer funcionar el action
    public void cleanCell(String cellPosition) {

        CellInterface cellInterfaceToRemove = getCell(cellPosition);
        cellInterfaces.remove(cellInterfaceToRemove);

    }

    public List<CellInterface> getCellsOrdered() {
        List<CellInterface> ordered = this.cellInterfaces;
        ordered.sort(new CellOrderComparator());
        return ordered;
    }

    @Override
    public void undo(Record record) {

        record.undo(this);

    }

    @Override
    public void redo(Record record) {

        record.redo(this);

    }

    private void validateNewFormatter(CellFormatter formatter) {

        try {

            formatter.valueAsString();

        } catch (InvalidFormatterException e) {

            throw new InvalidFormatterException();
        }

    }
}
