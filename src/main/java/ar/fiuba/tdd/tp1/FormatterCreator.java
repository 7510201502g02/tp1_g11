package ar.fiuba.tdd.tp1;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FormatterCreator extends Creator {

    LinkedList<CellFormatter> supportedFormatters = new LinkedList<>();

    public FormatterCreator() {

        supportedFormatters.addAll(Arrays
                .asList(new CellDecimalsFormatter(),
                        new CellDateFormatter(),
                        new CellSymbolFormatter()));
    }

    @Override
    public List<CellFormatter> getSupportedCreateElements() {

        return this.supportedFormatters;

    }

    @Override
    public CellFormatter getCreateInvalidElement() {

        return new CellInvalidFormatter();

    }

}