package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.exceptions.CreateWorkSpaceException;

import java.util.LinkedList;
import java.util.List;

public class WorkspacesManager<T extends Workspace> {

    private T workspaceType;
    private List<T> workSpaces = new LinkedList<>();

    public WorkspacesManager(T workspaceType) {
        this.workspaceType = workspaceType;
    }

    public void create(String workSpaceName) {

        if (!exist(workSpaceName)) {

            workSpaces.add(workspaceType.createNewWorkspace(workSpaceName));

        } else {

            throw new CreateWorkSpaceException("the Workspace with that name already exists");

        }

    }

    public void remove(String workSpaceName) {

        workSpaces.remove(this.get(workSpaceName));
    }

    public void changeNameFor(String newName, String workSpaceName) {

        if (!exist(newName) && exist(workSpaceName)) {

            this.get(workSpaceName).setName(newName);

        }

    }

    /*
        Rarisimo, con interfaces las funcionalidades de java 8 las tengo que castiar,
        con clases comunes no pasa.
    */

    private boolean exist(String workSpaceName) {

        return workSpaces.stream().anyMatch(it -> it.getName().equals(workSpaceName));
    }

    public T get(String workSpaceName) {

        return workSpaces.stream()
                .filter(it -> it.getName().equals(workSpaceName))
                .findFirst().get();
    }

    public List<String> names() {

        List<String> names = new LinkedList<>();
        workSpaces.stream().forEach(it -> names.add(it.getName()));

        return names;
    }

    public void add(T workspace) {

        if (!exist(workspace.getName())) {

            workSpaces.add(workspace);

        } else {

            throw new CreateWorkSpaceException("the Workspace with that name already exists");

        }
    }
}
