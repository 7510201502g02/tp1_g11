package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.CircularCellsReferencesException;

public class CellsReferenceManager {

    private Workbook workbookContext;

    public CellsReferenceManager(Workbook workbookContext) {

        this.workbookContext = workbookContext;

    }

    public String getValueFromReference(Position cellFromTokenPosition, Position contextCellPosition) {

        CellInterface cellInterfaceFromToken = getCell(cellFromTokenPosition);
        CellInterface contextCellInterface = getCell(contextCellPosition);

        if (!haveCircularReference(cellInterfaceFromToken, contextCellInterface)) {

            return cellInterfaceFromToken.valueAsString();

        } else {

            throw new CircularCellsReferencesException();
        }
    }

    private CellInterface getCell(Position cellPosition) {

        return workbookContext.getSheet(cellPosition.getSheetPosition()).getCell(cellPosition);

    }

    private boolean haveCircularReference(CellInterface cellInterfaceFromToken, CellInterface contextCellInterface) {

        return cellInterfaceFromToken.getCellsReferencesPositions().stream()
                .anyMatch(it -> it.samePosition(contextCellInterface.getPosition())
                        || machReferences(getCell(it), getCell(it))
                        || haveCircularReference(getCell(it), contextCellInterface));

    }

    private boolean machReferences(CellInterface cellInterfaceWithReferences, CellInterface cellInterfaceToMath) {

        return cellInterfaceWithReferences.getCellsReferencesPositions().stream()
                .anyMatch(it -> it.samePosition(cellInterfaceToMath.getPosition()));
    }

}
