package ar.fiuba.tdd.tp1.tokens;

import java.util.LinkedList;

public class BinaryExpressionTree implements ExpressionTree {

    private Token token;
    private ExpressionTree leftTree;
    private ExpressionTree rightTree;
    private ParenthesisSolver parenthesisSolver;

    public BinaryExpressionTree(TokensExpression tkExpression, ParenthesisSolver parenthesisSolver) {

        this.leftTree = new NullExpressionTree();
        this.rightTree = new NullExpressionTree();

        this.parenthesisSolver = parenthesisSolver;

        tkExpression = this.parenthesisSolver.mapParenthesis(tkExpression);

        if (tkExpression.hasOperations()) {
            this.fillTree(tkExpression);
        } else {
            this.token = tkExpression.getFirstToken();
        }

    }

    @Override
    public ExpressionTree replaceAllKeys() {

        if (this.parenthesisSolver.matches(this.token)) {
            return new BinaryExpressionTree(this.parenthesisSolver.getExpressionFor(this.token), this.parenthesisSolver);
        }
        leftTree = leftTree.replaceAllKeys();
        rightTree = rightTree.replaceAllKeys();

        return this;
    }

    private void fillTree(TokensExpression tkExpression) {

        this.token = tkExpression.getMainToken();

        TokensExpression leftExpression = tkExpression.getLeftSideOfTheExpression(this.token);
        this.leftTree = leftExpression.getMainToken().buildTree(leftExpression, this.parenthesisSolver);

        TokensExpression rightExpression = tkExpression.getRightSideOfTheExpression(this.token);
        this.rightTree = rightExpression.getMainToken().buildTree(rightExpression, this.parenthesisSolver);

    }

    @Override
    public void showTree() {

        System.out.println(token.toString());
        leftTree.showTree();
        rightTree.showTree();
    }

    @Override
    public String eval() {

        LinkedList<ExpressionTree> treeList = new LinkedList<ExpressionTree>();
        treeList.add(this.leftTree);
        treeList.add(this.rightTree);

        return this.token.evalWithTreeList(treeList);
    }

    @Override
    public Token getToken() {
        return this.token;
    }

}
