package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;

import java.util.LinkedList;

public class TextToken extends Token {

    String text;

    public TextToken() {

    }

    @Override
    public boolean valid(TransferPackage transferPackage) {

        return transferPackage.value.startsWith("'") && transferPackage.value.endsWith("'");

    }

    @Override
    public Token getCreatorElement(TransferPackage transferPackage) {

        return new TextToken(transferPackage.value.substring(1, transferPackage.value.length() - 1));

    }

    @Override
    public Token getToken() {

        return new TextToken(this.text);

    }


    @Override
    public String toScannerSymbol() {
        return "null";
    }

    private TextToken(String text) {
        this.text = text;

    }

    @Override
    public String toString() {
        return "'".concat(this.text).concat("'");
    }

    @Override
    public String getSyntaxIdentifier() {
        return "text";
    }

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();

        syntaxRule.addValidSyntaxToTheRight(new RightParenthesisToken());
        syntaxRule.addValidSyntaxToTheRight(new CommaToken());

        return syntaxRule;
    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {
        return this.text;
    }
}
