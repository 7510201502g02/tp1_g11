package ar.fiuba.tdd.tp1.tokens;


import ar.fiuba.tdd.tp1.StringFormatter;

import java.util.LinkedList;

public class PowToken extends TokenOperador {


    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {

        return StringFormatter.fmt((float) Math.pow(Float.parseFloat(treeList.get(0).eval()), Float.parseFloat(treeList.get(1).eval())));
    }

    @Override
    public Token getToken() {
        return new PowToken();
    }

    @Override
    public String toString() {
        return "^";
    }


    @Override
    public int getPriority() {
        return 2;
    }

}
