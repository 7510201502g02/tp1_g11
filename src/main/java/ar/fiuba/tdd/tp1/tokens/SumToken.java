package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

public class SumToken extends TokenFunction {

    @Override
    public Token getToken() {
        return new SumToken();
    }

    @Override
    public String toString() {
        return "SUM";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {

        float result = 0;

        for (String s : listOfValuesSplitted) {
            result = result + Float.parseFloat(s);
        }

        return StringFormatter.fmt(result);
    }
}
