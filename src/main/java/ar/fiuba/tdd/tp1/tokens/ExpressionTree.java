package ar.fiuba.tdd.tp1.tokens;

public interface ExpressionTree {

    void showTree();

    ExpressionTree replaceAllKeys();

    String eval();

    Token getToken();
}
