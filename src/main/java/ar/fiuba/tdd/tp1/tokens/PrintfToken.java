package ar.fiuba.tdd.tp1.tokens;

public class PrintfToken extends TokenFunction {

    @Override
    public Token getToken() {
        return new PrintfToken();
    }

    @Override
    public String toString() {
        return "PRINTF";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {

        String resultingText = listOfValuesSplitted[0];
        try {
            while (resultingText.contains("$")) {
                int position = resultingText.indexOf("$");
                String strArgumentIndex = resultingText.substring(position + 1, position + 2);
                int argumentIndex = Integer.parseInt(strArgumentIndex);
                resultingText = resultingText.replace("$" + strArgumentIndex, listOfValuesSplitted[argumentIndex + 1]);
            }
        } catch (Exception e) {
            return resultingText;
        }
        return resultingText;
    }

}
