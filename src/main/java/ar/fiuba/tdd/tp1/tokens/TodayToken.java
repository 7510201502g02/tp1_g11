package ar.fiuba.tdd.tp1.tokens;

import java.time.LocalDateTime;

public class TodayToken extends TokenFunction {

    @Override
    public Token getToken() {
        return new TodayToken();
    }

    @Override
    public String toString() {
        return "TODAY";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {
        return LocalDateTime.now().toString();
    }

}
