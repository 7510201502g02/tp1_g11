package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.CellInterface;
import ar.fiuba.tdd.tp1.CellsReferenceManager;
import ar.fiuba.tdd.tp1.Position;
import ar.fiuba.tdd.tp1.TransferPackage;

import java.util.LinkedList;


public class CellToken extends Token {

    private Position cellPosition;
    private CellInterface cellInterfaceContext;

    public CellToken() {
    }

    @Override
    public Token createFrom(TransferPackage transferPackageForTokenBuild) {

        // todo: que valide por el formato de las celdas:
        // dos casos: sheetname.A3 o A5
        String onlyCellRegularExpression = "([!a-zA-Z]+\\.)?[A-Z]+[\\d]+";

        if (transferPackageForTokenBuild.value.matches(onlyCellRegularExpression)) {

            transferPackageForTokenBuild.created();

            String cellReference = ifExistsRemoveFlagCharacter(transferPackageForTokenBuild);
            Position cellPosition = ifCellRefenceDontHaveContextAddContextFrom(cellReference, transferPackageForTokenBuild);

            return new CellToken(cellPosition, transferPackageForTokenBuild.context);

        } else {

            return new InvalidToken();

        }
    }

    @Override
    public Token getToken() {
        return new CellToken();
    }


    private String ifExistsRemoveFlagCharacter(TransferPackage transferPackageForTokenBuild) {

        String cellReference = transferPackageForTokenBuild.value;

        String completeCellReferenceExpression = "([!a-zA-Z]+\\.)[A-Z]+[\\d]+";
        if (cellReference.matches(completeCellReferenceExpression)) {

            return cellReference.substring(1);

        } else {

            return cellReference;

        }

    }

    private Position ifCellRefenceDontHaveContextAddContextFrom(String cellReference, TransferPackage transferPackageForTokenBuild) {

        Position cellPosition = new Position(cellReference);

        // en caso de no tener el contexto osea a que sheet pertenece
        if (cellPosition.getSheetPosition() == null) {

            cellPosition.setSheetPosition(transferPackageForTokenBuild.context.getPosition().getSheetPosition());

        }

        return cellPosition;

    }

    private CellToken(Position cellPosition, CellInterface cellInterfaceContext) {

        this.cellPosition = cellPosition;
        this.cellInterfaceContext = cellInterfaceContext;

    }

    @Override
    public String toRawString() {
        return "!" + this.toString();
    }

    @Override
    public String toString() {
        return this.cellPosition.toString();
    }

    @Override
    public String toScannerSymbol() {

        return "null";

    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {

        CellsReferenceManager referenceManager = cellInterfaceContext.getCellReferenceManager();
        return referenceManager.getValueFromReference(cellPosition, cellInterfaceContext.getPosition());
    }

    public Position getPosition() {
        return this.cellPosition;
    }

    public CellInterface getCellInterfaceContext() {
        return this.cellInterfaceContext;
    }

    // por implementar SytaxElement
    @Override
    public String getSyntaxIdentifier() {
        return "cell";
    }

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();

        syntaxRule.addRulesForNumerics();
        syntaxRule.addValidSyntaxToTheRight(new RangeToken());

        return syntaxRule;
    }
}