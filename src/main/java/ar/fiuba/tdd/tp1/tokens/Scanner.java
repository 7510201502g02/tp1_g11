package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.AliasManager;
import ar.fiuba.tdd.tp1.CellInterface;
import ar.fiuba.tdd.tp1.Creator;
import ar.fiuba.tdd.tp1.TransferPackage;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Scanner {

    Creator<Token> tokenCreator = new TokenCreator();

    public TokensExpression scan(TransferPackage transferPackage) {

        List<String> stringTokenList = this.tokenize(adaptExpression(transferPackage.value));
        stringTokenList = replaceAliases(stringTokenList);
        return createTokensExpressionWithContext(stringTokenList, transferPackage.context);

    }

    private List<String> replaceAliases(List<String> listOfPotentialAliases) {
        for (String token : listOfPotentialAliases) {
            if (AliasManager.getInstance().hasAlias(token)) {
                int indexOfAlias = listOfPotentialAliases.indexOf(token);
                listOfPotentialAliases.remove(token);
                listOfPotentialAliases.addAll(indexOfAlias, tokenize(AliasManager.getInstance().getElementByAlias(token)));
                return listOfPotentialAliases;
            }
        }
        return listOfPotentialAliases;
    }


    private LinkedList<String> tokenize(String expression) {

        return convertToListString(recognizeSupportedTokens(expression));
    }

    private StringTokenizer recognizeSupportedTokens(String expression) {

        expression = expression.replaceAll(" ", "");

        Iterator<Token> tokenIterator = (Iterator<Token>) tokenCreator.getSupportedCreateElements().iterator();
        while (tokenIterator.hasNext()) {
            Token token = tokenIterator.next();
            expression = expression.replaceAll(token.toScannerSymbol(), "º" + token.toScannerSymbol() + "º");
        }
        return new StringTokenizer(expression, "º");
    }

    private LinkedList<String> convertToListString(StringTokenizer stringTokenized) {

        LinkedList<String> stringTokenList = new LinkedList<String>();
        while (stringTokenized.hasMoreTokens()) {

            stringTokenList.add(stringTokenized.nextToken());
        }

        return stringTokenList;
    }

    private TokensExpression createTokensExpressionWithContext(List<String> stringTokenList, CellInterface context) {

        TokensExpression expressionTokens = new TokensExpression();

        Iterator<String> stringTokenListIterator = stringTokenList.iterator();

        while (stringTokenListIterator.hasNext()) {

            String stringToken = stringTokenListIterator.next();
            TransferPackage transferPackageForTokenBuild = new TransferPackage(stringToken, context);

            expressionTokens.addToTheExpression(tokenCreator.createFrom(transferPackageForTokenBuild));
        }

        return expressionTokens;
    }

    public String adaptExpression(String expression) {

        expression = expression.replaceAll(" ", "").replaceAll("--", "+").replaceAll("\\+-", "-").replaceAll("\\*-", "*(-1)*");

        return expression.replaceAll("\\/-", "/(-1)/");

    }

}
