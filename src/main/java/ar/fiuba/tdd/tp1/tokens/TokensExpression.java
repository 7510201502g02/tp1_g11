package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.Position;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class TokensExpression {

    private LinkedList<Token> tokens = new LinkedList<Token>();

    public void addToTheExpression(Token token) {
        tokens.addLast(token);
    }

    public Iterator<Token> getIterator() {
        return tokens.iterator();
    }

    // si el token no pertence te devuelve un una TokenExpression vacia
    public TokensExpression getLeftSideOfTheExpression(Token referenceToken) {
        return getSubExpression(0, tokens.indexOf(referenceToken));
    }

    // si el token no pertence te devuelve toda la TokenExpression
    public TokensExpression getRightSideOfTheExpression(Token referenceToken) {
        return getSubExpression(tokens.indexOf(referenceToken) + 1, tokens.size());
    }

    private TokensExpression getSubExpression(int initIndex, int finalIndex) {

        TokensExpression subExpression = new TokensExpression();
        for (int i = initIndex; i < finalIndex; i++) {
            subExpression.addToTheExpression(this.tokens.get(i));
        }

        return subExpression;
    }

    private Token head() {
        return tokens.getFirst();
    }

    private TokensExpression tail() {
        return this.getSubExpression(1, tokens.size());
    }

    public boolean hasParenthesis() {

        return ((!tokens.isEmpty()) && (this.head().isParenthesis() || this.tail().hasParenthesis()));
    }

    public boolean hasOperations() {

        return (!tokens.isEmpty()) && (this.head().isOperation() || this.tail().hasOperations());
    }

    private int getIndexOfFirstParenthesis() {
        if (head().isParenthesis()) {
            return 0;
        }
        return 1 + this.tail().getIndexOfFirstParenthesis();
    }

    private int getIndexOfClosingParenthesis(int parenthesisCount) {
        if (parenthesisCount == 0) {
            return 0;
        }
        return 1 + this.tail().getIndexOfClosingParenthesis(parenthesisCount + head().getParenthesisCount());
    }

    private void replaceExpression(TokensExpression expressionToReplace, KeyToken key) {
        this.tokens.add(tokens.indexOf(expressionToReplace.getFirstToken()), key);

        this.remove(expressionToReplace);
    }

    public TokensExpression replaceEnclosedExpression(KeyToken key) {
        int start = getIndexOfFirstParenthesis();
        int end = start + this.getRightSideOfTheExpression(this.tokens.get(start)).getIndexOfClosingParenthesis(1);

        TokensExpression innerExpression = this.getSubExpression(start + 1, end);

        this.replaceExpression(this.getSubExpression(start, end + 1), key);

        return innerExpression;
    }

    public LinkedList<TokensExpression> split() {
        LinkedList<TokensExpression> tkExpressionList = new LinkedList<TokensExpression>();

        while (!this.tokens.isEmpty()) {

            tkExpressionList.add(this.getSubExpression(0, this.getIndexOfComma()));
            this.remove(this.getSubExpression(0, this.getIndexOfComma()));

            if (!this.tokens.isEmpty()) {
                this.tokens.removeFirst();
            }
        }

        return tkExpressionList;
    }

    private int getIndexOfComma() {
        if (this.tokens.isEmpty() || head().isComma()) {
            return 0;
        }
        return 1 + this.tail().getIndexOfComma();
    }

    private void remove(TokensExpression expressionToRemove) {

        Iterator<Token> tokenIterator = expressionToRemove.getIterator();

        while (tokenIterator.hasNext()) {
            this.tokens.remove(tokenIterator.next());
        }
    }

    public Token getMainToken() {

        Iterator<Token> iterator = this.getIterator();
        Token token = new EmptyToken();
        while (iterator.hasNext()) {
            token = iterator.next().getMainToken(token);
        }

        return token;

    }

    public Token getTokenAt(int index) {
        return tokens.get(index);
    }

    public Token getFirstToken() {

        if (!this.tokens.isEmpty()) {
            return tokens.getFirst();
        }
        return new EmptyToken();
    }

    public int size() {
        return tokens.size();
    }

    // para las referencias ciculares

    public List<Position> getCellsReferencesPositions() {

        List<Position> cellsPositions = new LinkedList<>();

        tokens.stream().filter(it -> it.sameTokenType(new CellToken()))
                .forEach(it -> cellsPositions.add(new Position(it.toString())));

        return cellsPositions;
    }

    public String toRawString() {

        return this.toStringBasic((Token token) -> token.toRawString());
    }

    @Override
    public String toString() {

        return this.toStringBasic((Token token) -> token.toString());

    }

    public String toStringBasic(Function<Token, String> function) {

        String expressionAsString = "";
        Iterator<Token> tokenIterator = this.getIterator();

        while (tokenIterator.hasNext()) {
            expressionAsString = expressionAsString.concat(function.apply(tokenIterator.next()));
        }

        return expressionAsString;

    }
}
