package ar.fiuba.tdd.tp1.tokens;

public class EmptyToken extends Token {

    @Override
    public Token getToken() {
        return new EmptyToken();
    }

}
