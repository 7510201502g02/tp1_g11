package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.InvalidValue;

public class ExpressionSolver {

    public static final int ITERATIONS = 4;

    public String solveExpression(TokensExpression tkExpression) {

        ParenthesisSolver parenthesisSolver = new ParenthesisSolver();

        tkExpression = parenthesisSolver.mapParenthesis(tkExpression);

        ExpressionTree expressionTree = tkExpression.getMainToken().buildTree(tkExpression, parenthesisSolver);

        //Tree newExpressionTree = expressionTree.replaceAllKeys();

        /*while (!expressionTree.equals(newExpressionTree)) {
               expressionTree = newExpressionTree;
               newExpressionTree = expressionTree.replaceAllKeys();
        }*/

        // Esto debe ajustarse usando una comparacion como la propuesta arriba. Falta terminar.

        for (int i = 0; i < ITERATIONS; i++) {
            expressionTree = expressionTree.replaceAllKeys();
        }

        try {
            return expressionTree.eval();

        } catch (NumberFormatException e) {

            return new InvalidValue("Error:BAD_FORMULA").valueAsString();

        }
    }
}
