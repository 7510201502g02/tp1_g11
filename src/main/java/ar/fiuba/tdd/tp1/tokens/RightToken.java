package ar.fiuba.tdd.tp1.tokens;

public class RightToken extends TokenFunction {

    @Override
    public Token getToken() {
        return new RightToken();
    }

    @Override
    public String toString() {
        return "RIGHT";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {
        return listOfValuesSplitted[0].substring(Math.max(listOfValuesSplitted[0].length() - Integer.parseInt(listOfValuesSplitted[1]), 0), listOfValuesSplitted[0].length());
    }

}
