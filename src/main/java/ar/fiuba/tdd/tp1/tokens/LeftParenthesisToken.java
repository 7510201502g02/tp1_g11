package ar.fiuba.tdd.tp1.tokens;


public class LeftParenthesisToken extends Token {

    @Override
    public Token getToken() {
        return new LeftParenthesisToken();
    }

    @Override
    public String toString() {
        return "(";
    }

    @Override
    public boolean isParenthesis() {
        return true;
    }

    @Override
    public int getParenthesisCount() {
        return 1;
    }

    public String toScannerSymbol() {
        return "\\" + toString();
    }

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();

        syntaxRule.addAllValues();
        syntaxRule.addAllFunctions();
        syntaxRule.addValidSyntaxToTheRight(new LeftParenthesisToken());
        syntaxRule.addValidSyntaxToTheRight(new TextToken());

        syntaxRule.addValidSyntaxToTheRight(new PlusToken());
        syntaxRule.addValidSyntaxToTheRight(new MinusToken());

        return syntaxRule;

    }

}
