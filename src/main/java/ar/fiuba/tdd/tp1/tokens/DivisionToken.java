package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

import java.util.LinkedList;

public class DivisionToken extends TokenOperador {

    @Override
    public Token getToken() {
        return new DivisionToken();
    }

    @Override
    public String toString() {

        return "/";
    }

    @Override
    public int getPriority() {
        return 2;
    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {
        return StringFormatter.fmt(Float.parseFloat(treeList.get(0).eval()) / Float.parseFloat(treeList.get(1).eval()));
    }

}
