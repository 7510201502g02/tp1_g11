package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.CellInterface;
import ar.fiuba.tdd.tp1.CellsReferenceManager;
import ar.fiuba.tdd.tp1.ColumnMapper;
import ar.fiuba.tdd.tp1.Position;

import java.util.LinkedList;

public class RangeToken extends Token {

    @Override
    public Token getToken() {
        return new RangeToken();
    }


    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();
        syntaxRule.addValidSyntaxToTheRight(new CellToken());

        return syntaxRule;
    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {

        int firstColumn = ColumnMapper.toColumnNumber(((CellToken) treeList.get(0).getToken()).getPosition().getColumn());
        int firstRow = Integer.parseInt(((CellToken) treeList.get(0).getToken()).getPosition().getRow());
        int secondColumn = ColumnMapper.toColumnNumber(((CellToken) treeList.get(1).getToken()).getPosition().getColumn());
        int secondRow = Integer.parseInt(((CellToken) treeList.get(1).getToken()).getPosition().getRow());

        CellInterface context = ((CellToken) treeList.get(0).getToken()).getCellInterfaceContext();
        CellsReferenceManager refManager = context.getCellReferenceManager();

        String result = "";

        for (int i = Math.min(firstColumn, secondColumn); i <= Math.max(firstColumn, secondColumn); i++) {
            for (int j = Math.min(firstRow, secondRow); j <= Math.max(firstRow, secondRow); j++) {
                String sheetPosition = context.getPosition().getSheetPosition().concat(".");
                String position = sheetPosition.concat(ColumnMapper.toColumnKey(i).concat(String.valueOf(j)));
                String value = refManager.getValueFromReference(new Position(position), context.getPosition());
                result = result.concat(value).concat(",");
            }
        }

        return result.substring(0, result.length() - 1);
    }

    @Override
    public int getPriority() {
        return 2;
    }

    @Override
    public String toString() {
        return ":";
    }

    @Override
    public boolean isOperation() {
        return true;
    }
}
