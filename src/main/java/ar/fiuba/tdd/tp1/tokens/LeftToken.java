package ar.fiuba.tdd.tp1.tokens;

public class LeftToken extends TokenFunction {

    @Override
    public Token getToken() {
        return new LeftToken();
    }

    @Override
    public String toString() {
        return "LEFT";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {
        return listOfValuesSplitted[0].substring(0, Math.min(listOfValuesSplitted[0].length(), Integer.parseInt(listOfValuesSplitted[1])));
    }

}
