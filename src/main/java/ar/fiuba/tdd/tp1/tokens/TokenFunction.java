package ar.fiuba.tdd.tp1.tokens;

import java.util.LinkedList;

public abstract class TokenFunction extends Token {

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();
        syntaxRule.addValidSyntaxToTheRight(new LeftParenthesisToken());

        return syntaxRule;

    }

    @Override
    public boolean isOperation() {
        return true;
    }

    @Override
    public ExpressionTree buildTree(TokensExpression tkExpression, ParenthesisSolver parenthesisSolver) {
        return new MultiExpressionTree(tkExpression, parenthesisSolver);
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    public String toScannerSymbol() {
        return toString();
    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {

        String listOfValues = "";

        for (ExpressionTree expTree : treeList) {
            listOfValues = listOfValues.concat(expTree.eval()).concat(",");
        }

        String[] listOfValuesSplitted = listOfValues.split(",");

        return this.result(listOfValuesSplitted);

    }

    public abstract String result(String[] listOfValuesSplitted);

}
