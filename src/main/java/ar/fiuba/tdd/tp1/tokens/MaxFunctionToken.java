package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

public class MaxFunctionToken extends TokenFunction {

    @Override
    public String toString() {
        return "MAX";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {

        float result = Float.parseFloat(listOfValuesSplitted[0]);

        for (String s : listOfValuesSplitted) {
            result = Math.max(Float.parseFloat(s), result);
        }

        return StringFormatter.fmt(result);
    }

    @Override
    public Token getToken() {
        return new MaxFunctionToken();
    }
}
