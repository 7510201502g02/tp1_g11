package ar.fiuba.tdd.tp1.tokens;

import java.util.HashMap;

public class ParenthesisSolver {

    HashMap<Token, TokensExpression> expressionsMap;

    public ParenthesisSolver() {
        this.expressionsMap = new HashMap<Token, TokensExpression>();
    }

    public TokensExpression mapParenthesis(TokensExpression tkExpression) {

        while (tkExpression.hasParenthesis()) {
            KeyToken key = new KeyToken();

            this.expressionsMap.put(key, tkExpression.replaceEnclosedExpression(key));
        }

        return tkExpression;
    }

    public boolean matches(Token potentialKey) {
        return expressionsMap.containsKey(potentialKey);
    }

    public TokensExpression getExpressionFor(Token key) {
        return expressionsMap.get(key);
    }

}
