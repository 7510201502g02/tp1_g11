package ar.fiuba.tdd.tp1.tokens;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SyntaxRule {

    LinkedList<Token> rightValidSyntaxElements = new LinkedList<>();

    public void addValidSyntaxToTheRight(Token element) {
        this.rightValidSyntaxElements.add(element);
    }

    public void addALLValidSyntaxToTheRight(List<Token> validTokens) {
        this.rightValidSyntaxElements.addAll(validTokens);
    }

    public Boolean isValidSyntax(Token element, Token leftElement, Token rightElement) {
        return leftElement.getSyntaxRule().isValidSyntaxToTheRight(element) && this.isValidSyntaxToTheRight(rightElement);
    }

    public Boolean isValidSyntaxToTheRight(Token element) {

        Boolean isValid = false;
        Iterator<Token> validElementIterator = this.rightValidSyntaxElements.iterator();

        while (validElementIterator.hasNext()) {
            if (validElementIterator.next().sameTokenType(element)) {
                isValid = true;
            }
        }

        return isValid;
    }

    public void addAllOperations() {

        this.addALLValidSyntaxToTheRight(Arrays
                .asList(
                        new PlusToken(),
                        new MinusToken(),
                        new DivisionToken(),
                        new MultiplyToken(),
                        new PowToken()
                ));
    }

    public void addAllFunctions() {

        this.addALLValidSyntaxToTheRight(Arrays
                .asList(
                        new SumToken(),
                        new SubtractionFunctionToken(),
                        new AverageFunctionToken(),
                        new ConcatToken(),
                        new MaxFunctionToken(),
                        new MinFunctionToken(),
                        new RightParenthesisToken()
                ));
    }

    public void addAllValues() {
        this.addValidSyntaxToTheRight(new NumericToken());
        this.addValidSyntaxToTheRight(new CellToken());
    }

    // extra para simplificar codigo
    public void addRulesForNumerics() {

        this.addAllOperations();
        this.addValidSyntaxToTheRight(new RightParenthesisToken());
        this.addValidSyntaxToTheRight(new CommaToken());
    }

}



