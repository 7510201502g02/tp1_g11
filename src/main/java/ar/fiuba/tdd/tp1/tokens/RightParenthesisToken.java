package ar.fiuba.tdd.tp1.tokens;

public class RightParenthesisToken extends Token {

    @Override
    public Token getToken() {

        return new RightParenthesisToken();
    }

    @Override
    public String toString() {
        return ")";
    }

    @Override
    public int getParenthesisCount() {
        return -1;
    }

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();

        syntaxRule.addAllOperations();
        syntaxRule.addValidSyntaxToTheRight(new RightParenthesisToken());
        syntaxRule.addValidSyntaxToTheRight(new CommaToken());

        return syntaxRule;
    }
}
