package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

import java.util.LinkedList;

public class MinusToken extends TokenOperador {

    @Override
    public Token getToken() {
        return new MinusToken();
    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {
        return StringFormatter.fmt(Float.parseFloat(treeList.get(0).eval()) - Float.parseFloat(treeList.get(1).eval()));
    }

    @Override
    public int getPriority() {
        return 4;
    }

    @Override
    public String toString() {
        return "-";
    }

}
