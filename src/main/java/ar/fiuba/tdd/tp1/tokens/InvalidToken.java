package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaExpressionException;

import java.util.LinkedList;

public class InvalidToken extends Token {

    @Override
    public Token createFrom(TransferPackage transferPackageForTokenBuild) {
        return this.getToken();
    }

    @Override
    public Token getToken() {
        return new InvalidToken();
    }

    @Override
    public String toString() {
        return "null";
    }

    @Override
    public ExpressionTree buildTree(TokensExpression tkExpression, ParenthesisSolver parenthesisSolver) {
        return new NullExpressionTree();
    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {

        throw new InvalidFormulaExpressionException();
    }

    @Override
    public SyntaxRule getSyntaxRule() {
        return new SyntaxRule(); // no tiene ningun valido!
    }

    @Override
    public int getPriority() {
        return -1;
    }

}
