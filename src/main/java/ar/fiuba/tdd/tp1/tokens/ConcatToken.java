package ar.fiuba.tdd.tp1.tokens;

public class ConcatToken extends TokenFunction {

    @Override
    public Token getToken() {
        return new ConcatToken();
    }

    @Override
    public String toString() {
        return "CONCAT";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {

        String resultingText = "";

        for (String s : listOfValuesSplitted) {
            resultingText = resultingText.concat(s);
        }

        return resultingText;
    }

}
