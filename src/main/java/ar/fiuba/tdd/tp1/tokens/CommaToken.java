package ar.fiuba.tdd.tp1.tokens;

public class CommaToken extends Token {

    @Override
    public Token getToken() {
        return new CommaToken();
    }

    @Override
    public String toString() {
        return ",";
    }

    @Override
    public boolean isComma() {
        return true;
    }

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();
        syntaxRule.addAllValues();
        syntaxRule.addAllFunctions();
        syntaxRule.addValidSyntaxToTheRight(new TextToken());

        return syntaxRule;
    }
}
