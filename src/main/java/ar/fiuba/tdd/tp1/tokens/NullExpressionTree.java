package ar.fiuba.tdd.tp1.tokens;


public class NullExpressionTree implements ExpressionTree {

    @Override
    public void showTree() {

    }

    @Override
    public ExpressionTree replaceAllKeys() {
        return this;
    }

    @Override
    public Token getToken() {
        return new EmptyToken();
    }

    @Override
    public String eval() {
        return "0";
    }

}
