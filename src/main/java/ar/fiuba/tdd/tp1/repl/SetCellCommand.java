package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.Action;
import ar.fiuba.tdd.tp1.SetCellValueAction;
import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Workbook;

class SetCellCommand extends ConsoleCommand {

    private String workbookName;
    private String worksheetName;
    private String position;
    private String value;

    SetCellCommand(ConsoleCommand nextCommand) {
        this.identifier = "setCell";
        this.next = nextCommand;
    }

    @Override
    String getDescription() {
        return this.identifier + " : Example of use; " + this.identifier
                + " 'workbookName.worksheetName cellposition cellvalue' ";
    }

    @Override
    void apply(CommandTransferPackage transfer) {
        getParsedVariables(transfer);

        Workbook workbookToOperate = transfer.workBooks.get(this.workbookName);
        Sheet sheetOperate = workbookToOperate.getSheet(this.worksheetName);
        Action addCellAction = new SetCellValueAction(sheetOperate, this.position, this.value, transfer.recordsManager);
        addCellAction.execute();
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {
        System.out.println("cell: " + this.position + " was set on : " + this.workbookName + "."
                + this.worksheetName + " with value: " + this.value);
    }

    void getParsedVariables(CommandTransferPackage transfer) {
        String[] nameSplitted = transfer.getInstructionSplitted()[1].split("\\."); // [workbookname, sheetname]
        this.workbookName = nameSplitted[0];
        this.worksheetName = nameSplitted[1];
        this.position = transfer.getInstructionSplitted()[2]; // [cellposition]
        this.value = transfer.getInstructionSplitted()[3]; // [cellValue]
    }
}
