package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.AliasManager;
import ar.fiuba.tdd.tp1.exceptions.BadRangeException;

public class NamedRangeCommand extends ConsoleCommand {
    public NamedRangeCommand(ConsoleCommand exitCommand) {

        this.identifier = "saveAlias";
        this.next = exitCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {
        try {
            AliasManager.getInstance().saveRangeAlias(transfer.getInstructionSplitted()[0],transfer.getInstructionSplitted()[1]);
        } catch (BadRangeException e) {
            System.out.println("invalid range");
        }
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

    }

    @Override
    String getDescription(){
        return this.identifier + " : Example of use; " + this.identifier + " 'range' 'alias' ";
    }
}
