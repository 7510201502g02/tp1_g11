package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.RecordsManager;

import java.util.function.Consumer;

abstract class ActionCommand extends ConsoleCommand {

    protected Consumer<RecordsManager> action;
    protected String actionMessage;

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println(actionMessage);
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        action.accept(transfer.recordsManager);
    }
}
