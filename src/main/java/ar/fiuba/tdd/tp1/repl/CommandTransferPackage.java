package ar.fiuba.tdd.tp1.repl;


import ar.fiuba.tdd.tp1.RecordsManager;
import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;

class CommandTransferPackage {

    WorkspacesManager<Workbook> workBooks;
    RecordsManager recordsManager;
    String instruction;

    CommandTransferPackage(WorkspacesManager<Workbook> workBooks, RecordsManager recordsManager) {

        this.workBooks = workBooks;
        this.recordsManager = recordsManager;

    }

    String[] getInstructionSplitted() {

        return this.instruction.split(" ");

    }

}
