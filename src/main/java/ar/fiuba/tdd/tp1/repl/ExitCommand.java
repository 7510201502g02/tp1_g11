package ar.fiuba.tdd.tp1.repl;

import static ar.fiuba.tdd.tp1.repl.ConsoleCommander.EXIT;

class ExitCommand extends ConsoleCommand {

    ExitCommand() {
        this.identifier = EXIT;
        this.next = null;
    }


    ExitCommand(ConsoleCommand nextCommand) {

        this.identifier = EXIT;
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        transfer.instruction = this.identifier;

    }

    String getDescription() {

        return this.identifier + " : command to exit from console";

    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println("Back to the future..");

    }

}
