package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.CellInterface;

import java.util.StringJoiner;

abstract class ShowCellCommand extends ConsoleCommand {

    protected StringJoiner stringJoiner;

    void showCell(String workBookName, CellInterface cell) {

        StringJoiner joiner = new StringJoiner(".");
        joiner.add(workBookName);
        joiner.add(cell.getPosition().toString());

        stringJoiner = new StringJoiner("->");

        stringJoiner.add(joiner.toString());
        stringJoiner.add(cell.valueAsString());

        System.out.println(stringJoiner.toString());
    }

}
