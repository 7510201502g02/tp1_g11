package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.RecordsManager;

class UndoCommand extends ActionCommand {

    UndoCommand(ConsoleCommand nextCommand) {

        this.identifier = "undo";
        this.next = nextCommand;
        this.actionMessage = "LAST ACTION UNDONE.";
        this.action = (RecordsManager rec) -> rec.undo();
    }

    @Override
    String getDescription() {

        return this.identifier + " : undo last action.";

    }
}
