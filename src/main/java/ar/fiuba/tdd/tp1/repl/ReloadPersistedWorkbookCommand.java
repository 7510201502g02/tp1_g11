package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.persisting.Importer;
import ar.fiuba.tdd.tp1.persisting.JsonImport;

class ReloadPersistedWorkbookCommand extends ConsoleCommand {

    @Override
    void apply(CommandTransferPackage transfer) {

        Importer importer = new Importer(new JsonImport(), transfer.workBooks);
        importer.importFile(transfer.getInstructionSplitted()[1], "", "");
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println("Workbook reloaded from file: " + transfer.getInstructionSplitted()[1]);
    }

    ReloadPersistedWorkbookCommand(ConsoleCommand nextCommand) {
        this.identifier = "reloadWorkbook";
        this.next = nextCommand;
    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'fileName' ";
    }
}
