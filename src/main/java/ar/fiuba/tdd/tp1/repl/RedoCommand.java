package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.RecordsManager;


class RedoCommand extends ActionCommand {

    @Override
    String getDescription() {

        return this.identifier + " : redo last undone action.";

    }

    RedoCommand(ConsoleCommand nextCommand) {

        this.identifier = "redo";
        this.next = nextCommand;
        this.actionMessage = "REDONE LAST UNDONE ACTION.";
        this.action = (RecordsManager rec) -> rec.redo();
    }
}
