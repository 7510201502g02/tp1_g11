package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.Sheet;

class SetCellTypeCommand extends ConsoleCommand {
    @Override
    void apply(CommandTransferPackage transfer) {

        String[] id = transfer.getInstructionSplitted()[1].split("\\.");
        Sheet sheetToOperate = transfer.workBooks.get(id[0]).getSheet(id[1]);
        sheetToOperate.setValueTypeTo(transfer.getInstructionSplitted()[2], castTypeIdentifier(transfer.getInstructionSplitted()[3]));
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {
        System.out.println("TYPE APPLIED.");
    }

    @Override
    String getDescription() {
        return this.identifier + " : Example of use; " + this.identifier
                + " 'workbookName.worksheetName cellposition type' ";
    }

    SetCellTypeCommand(ConsoleCommand next) {

        this.identifier = "setCellType";
        this.next = next;
    }

    private String castTypeIdentifier(String type) {

        if (type.equals("Date")) {

            type = type.replaceAll("D", "d");

        } else if (type.equals("Currency")) {

            type = "number";

        } else if (type.equals("String")) {

            type = "text";

        }

        return type;
    }

}
