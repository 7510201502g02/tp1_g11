package ar.fiuba.tdd.tp1.repl;

import java.util.List;
import java.util.StringJoiner;

abstract class NamesCommand extends ConsoleCommand {

    protected List<String> names;

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println(workspace() + " names: " + namesWithFormat(names));
    }

    protected String namesWithFormat(List<String> names) {

        StringJoiner nameListWithFormat = new StringJoiner(",", "[", "]");
        names.forEach((String name) -> nameListWithFormat.add(name));

        return nameListWithFormat.toString();

    }

    protected abstract String workspace();
}
