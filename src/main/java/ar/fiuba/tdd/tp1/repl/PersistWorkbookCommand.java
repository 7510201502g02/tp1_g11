package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.persisting.JsonPersistence;
import ar.fiuba.tdd.tp1.persisting.Persistor;

class PersistWorkbookCommand extends ConsoleCommand {

    @Override
    void apply(CommandTransferPackage transfer) {

        Workbook workbook = transfer.workBooks.get(transfer.getInstructionSplitted()[1]);
        Persistor persistor = new Persistor(new JsonPersistence(), workbook);
        persistor.persistWorkBook(transfer.getInstructionSplitted()[2]);
    }

    PersistWorkbookCommand(ConsoleCommand nextCommand) {
        this.identifier = "persistWorkbook";
        this.next = nextCommand;
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println("Workbook: " + transfer.getInstructionSplitted()[1] + " persisted.");
        System.out.println("File: " + transfer.getInstructionSplitted()[2]);
    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'workbookName' 'fileName' ";
    }
}
