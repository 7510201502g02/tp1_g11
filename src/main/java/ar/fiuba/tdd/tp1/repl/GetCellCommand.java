package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.CellInterface;

class GetCellCommand extends ShowCellCommand {

    private String workBookName;
    private CellInterface cell;

    GetCellCommand(ConsoleCommand nextCommand) {
        this.identifier = "getCell";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        String[] nameSplitted = transfer.getInstructionSplitted()[1].split("\\."); // [workbookname, sheetname]

        workBookName = nameSplitted[0];
        String sheetName = nameSplitted[1];
        String cellPosition = transfer.getInstructionSplitted()[2];

        cell = transfer.workBooks.get(workBookName).getSheet(sheetName).getCell(cellPosition);

    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        this.showCell(workBookName, cell);

    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'workbookName' 'sheetName' 'cellPosition' ";

    }
}
