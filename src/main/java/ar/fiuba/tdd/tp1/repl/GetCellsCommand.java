package ar.fiuba.tdd.tp1.repl;


import ar.fiuba.tdd.tp1.CellInterface;

import java.util.List;

class GetCellsCommand extends ShowCellCommand {


    GetCellsCommand(ConsoleCommand nextCommand) {
        this.identifier = "getCells";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        List<String> workBookNameList = transfer.workBooks.names();

        for (String workBookName : workBookNameList) {
            List<String> sheetNameList = transfer.workBooks.get(workBookName).sheetsNames();

            for (String sheetName : sheetNameList) {
                List<CellInterface> cells = transfer.workBooks.get(workBookName).getSheet(sheetName).getCellsOrdered();

                System.out.println("");

                for (CellInterface cell : cells) {

                    this.showCell(workBookName, cell);

                }
            }
        }
    }

    @Override
    String getDescription() {

        return this.identifier + " : give the cells of a workbook";

    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

    }


}
