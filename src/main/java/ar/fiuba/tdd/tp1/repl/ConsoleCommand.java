package ar.fiuba.tdd.tp1.repl;

abstract class ConsoleCommand {

    protected ConsoleCommand next;
    protected String identifier;

    void setNext(ConsoleCommand next) {

        this.next = next;

    }

    ConsoleCommand getNext() {

        return this.next;

    }

    /*
        se aplica si puede, sino sigue la cadena
    */
    void run(CommandTransferPackage transfer) {

        if (transfer.getInstructionSplitted()[0].equals(identifier)) {

            try {

                apply(transfer);
                showStatus(transfer);

            } catch (Exception exception) {

                System.out.println("");
                System.out.println("Error Trying to run command");
                System.out.println("");
                System.out.println(exception.getMessage());
                System.out.println("");
            }

        } else {

            next.run(transfer);

        }
    }

    abstract void apply(CommandTransferPackage transfer);

    String getDescription() {

        return this.identifier;

    }

    void showStatus(CommandTransferPackage transfer) {

        System.out.println("");
        showStatusMessage(transfer);
        System.out.println("");

    }

    abstract void showStatusMessage(CommandTransferPackage transfer);


}
