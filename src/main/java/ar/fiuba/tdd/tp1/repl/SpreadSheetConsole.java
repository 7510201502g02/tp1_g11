package ar.fiuba.tdd.tp1.repl;

public class SpreadSheetConsole {

    /*
        para compilar el jar:  ~$ ./gradlew fatjar

        para correrlo, ir a la carpeta /build/libs
        y ejecutar: ~$ java -jar tp1-all-1.0-SNAPSHOT.jar

     */

    public static void main(String[] args) {

        new ConsoleCommander().run();

    }

}

