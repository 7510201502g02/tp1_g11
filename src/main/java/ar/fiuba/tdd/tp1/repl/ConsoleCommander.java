package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.RecordsManager;
import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleCommander {

    static final String EXIT = "exit";
    private ConsoleCommand commands;

    public ConsoleCommander() {

        // armando la cadena
        commands = new CreateWorkbookCommand(
                new CreateWorksheetCommand(
                        new WorkbooksNamesCommand(
                                new WorksheetNamesCommand(
                                        new SetCellCommand(
                                                new GetCellsCommand(
                                                        new UndoCommand(
                                                                new RedoCommand(
                                                                        new PersistWorkbookCommand(
                                                                                new ReloadPersistedWorkbookCommand(
                                                                                        new SetCellFormatterCommand(
                                                                                                new SetCellTypeCommand(
                                                                                                        new GetCellCommand(
                                                                                                                new ExportSheetToCSVCommand(
                                                                                                                        new ImportCsvIntoASheetCommand(
                                                                                                                                new NamedRangeCommand(
                                                                                                                                        new ExitCommand()))))))))))))))));

        /*
            Help tiene que ir despues para poder tener las descripciones.
            Invalid al final por como esta implementado!
         */

        this.getCommandAtPosition(this.size() - 1).setNext(new HelpCommand(commandsDescriptions(), new InvalidCommand()));
    }

    public void run() {

        CommandTransferPackage transfer = new CommandTransferPackage(new WorkspacesManager<>(new Workbook()), new RecordsManager());
        transfer.instruction = "";

        while (!transfer.instruction.equals(EXIT)) {

            transfer.instruction = new Scanner(System.in, "utf-8").nextLine();//System.console().readLine("SpreadSheet-Shell ~$ ");
            commands.run(transfer);

        }
    }

    private List<String> commandsDescriptions() {

        List<String> commandsDescriptions = new ArrayList<>(0);

        for (int i = 0; i < size(); i++) {
            commandsDescriptions.add(getCommandAtPosition(i).getDescription());
        }

        return commandsDescriptions;
    }

    /*
        para poder recuperar un elemento especifico en la cadena
        si la cadena esta vacia devuelve null.
    */
    private ConsoleCommand getCommandAtPosition(int position) {

        ConsoleCommand commandIterator = commands;

        for (int i = 1; i <= position; i++) {

            commandIterator = commandIterator.getNext();
        }

        return commandIterator;
    }

    private int size() {

        ConsoleCommand command = commands;

        if (command == null) {
            return -1;
        }

        int counter = 0;
        while (command != null) {
            counter++;
            command = command.getNext();
        }

        return counter;
    }


}
