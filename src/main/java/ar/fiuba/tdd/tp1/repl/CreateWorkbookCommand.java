package ar.fiuba.tdd.tp1.repl;


class CreateWorkbookCommand extends ConsoleCommand {

    CreateWorkbookCommand(ConsoleCommand nextCommand) {

        this.identifier = "createWorkbook";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        transfer.workBooks.create(transfer.getInstructionSplitted()[1]);
    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'workbookName' ";

    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println("workbook: " + transfer.getInstructionSplitted()[1] + " was created");

    }
}
