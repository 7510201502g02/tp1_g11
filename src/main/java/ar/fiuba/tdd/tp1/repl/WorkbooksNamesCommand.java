package ar.fiuba.tdd.tp1.repl;

class WorkbooksNamesCommand extends NamesCommand {

    WorkbooksNamesCommand(ConsoleCommand nextCommand) {

        this.identifier = "workbooksNames";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        names = transfer.workBooks.names();

    }

    @Override
    String getDescription() {

        return this.identifier + " : give the list of created workbooks";

    }

    @Override
    protected String workspace() {

        return "workbooks";

    }
}




