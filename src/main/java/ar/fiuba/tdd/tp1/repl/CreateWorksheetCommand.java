package ar.fiuba.tdd.tp1.repl;


import ar.fiuba.tdd.tp1.Action;
import ar.fiuba.tdd.tp1.CreateWorkSheetAction;
import ar.fiuba.tdd.tp1.Workbook;

class CreateWorksheetCommand extends ConsoleCommand {

    private String workbookName;
    private String worksheetName;

    CreateWorksheetCommand(ConsoleCommand nextCommand) {

        this.identifier = "createWorksheet";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        initVariables(transfer);

        Workbook workbookToOperate = transfer.workBooks.get(this.workbookName);
        Action action = new CreateWorkSheetAction(workbookToOperate, this.worksheetName, transfer.recordsManager);

        action.execute();

    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'workbookName.worksheetName' ";

    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println("worksheet: " + worksheetName + " was created on workbook: " + workbookName);

    }

    private void initVariables(CommandTransferPackage transfer) {

        String[] nameSplitted = transfer.getInstructionSplitted()[1].split("\\."); // [workbookname, sheetname]
        this.workbookName = nameSplitted[0];
        this.worksheetName = nameSplitted[1];
    }
}
