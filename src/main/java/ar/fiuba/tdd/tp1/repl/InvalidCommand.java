package ar.fiuba.tdd.tp1.repl;

// TIENE QUE IR AL FINAL EN LA CADENA
class InvalidCommand extends ConsoleCommand {

    InvalidCommand() {

        this.identifier = "";
        this.next = null;
    }

    @Override
    void run(CommandTransferPackage transfer) {

        if (!transfer.getInstructionSplitted()[0].equals(identifier)) {

            showStatus(transfer);

        }
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        // do nothing

    }

    @Override
    String getDescription() {

        return "";

    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println("invalid command");

    }

}
