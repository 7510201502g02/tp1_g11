package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.persisting.CsvImport;
import ar.fiuba.tdd.tp1.persisting.Importer;

public class ImportCsvIntoASheetCommand extends ConsoleCommand {

    ImportCsvIntoASheetCommand(ConsoleCommand nextCommand) {
        this.identifier = "importCsvIntoSheet";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        String workbookName = transfer.getInstructionSplitted()[1].split("\\.")[0];
        String sheetName = transfer.getInstructionSplitted()[1].split("\\.")[1];

        Importer importer = new Importer(new CsvImport(), transfer.workBooks);

        importer.importFile(transfer.getInstructionSplitted()[2], workbookName, sheetName);
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.out.println("Csv imported into a Sheet");
    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'workbookName.worksheetName' 'csvfileName' ";
    }
}


