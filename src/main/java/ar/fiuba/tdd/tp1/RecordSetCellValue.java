package ar.fiuba.tdd.tp1;

public class RecordSetCellValue implements Record {

    private String cellPosition;
    private Value oldValue;
    private Value newValue;
    private boolean oldCellHasValue;


    public RecordSetCellValue(String cellPosition, Value oldValue, Value newValue, boolean oldCellHasValue) {

        this.cellPosition = cellPosition;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.oldCellHasValue = oldCellHasValue;


    }

    @Override
    public void undo(Reversible reversible) {

        Sheet sheet = (Sheet) reversible;

        if (oldCellHasValue) {

            sheet.getCell(cellPosition).setValue(oldValue);

        } else {

            sheet.cleanCell(cellPosition);

        }

    }

    @Override
    public void redo(Reversible reversible) {

        Sheet sheet = (Sheet) reversible;

        /*
            antes hace getCell, y si habia retrocedido lo suficiente
            con el undo, tiraba error porque no la encontraba
        */
        sheet.setCellValue(cellPosition, newValue.getRawValue()); // ojo que usa la inferencia de tipo

    }


}

