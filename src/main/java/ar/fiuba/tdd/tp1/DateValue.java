package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InvalidValueException;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/*
    Espera valores de la forma: "yyyy-MM-ddTHH:mm:ssZ"
    cualquier otra cosa que le pongas no nos hacemos cargo de que ande
    o aplique bien los formatos.
*/

public class DateValue extends Value {

    private void init(String rawValue) {
        this.valueType = "date";
        this.rawValue = rawValue;
    }

    public DateValue() {

        init(null);
    }


    private DateValue(TransferPackage transfer) {

        init(transfer.value);
    }

    public Double valueAsDouble() {

        throw new InvalidValueException("I am date");

    }

    @Override
    public String valueAsString() {

        return this.rawValue;

    }

    @Override
    public boolean internalValid(TransferPackage transfer) {

        try {

            DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(transfer.value);
            return true;

        } catch (DateTimeParseException e) {

            return false;

        }

    }

    @Override
    public Value getCreatorElement(TransferPackage transferPackage) {

        if (internalValid(transferPackage)) {

            return new DateValue(transferPackage);

        } else {

            return new InvalidValue("Error:BAD_DATE");
        }

    }

}

