package ar.fiuba.tdd.tp1;


public abstract class Workspace implements Reversible {

    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // para poder reutilizar codigo
    public abstract <T extends Workspace> T createNewWorkspace(String newName);

}
