package ar.fiuba.tdd.tp1;

public class NumberValue extends Value {

    private String onlyNumbers = "^[-+]?[0-9]*\\.?[0-9]+$";
    private static String VALUE_TYPE_NUMBER = "number";

    private void init(String rawValue) {
        this.rawValue = rawValue;
        this.valueType = VALUE_TYPE_NUMBER;
    }

    public NumberValue() {
        init(null);
    }

    private NumberValue(String value) {

        init(value);
    }

    @Override
    public boolean internalValid(TransferPackage transferPackage) {

        return transferPackage.value.matches(onlyNumbers);

    }

    @Override
    public Value getCreatorElement(TransferPackage transferPackage) {

        if (internalValid(transferPackage)) {

            return new NumberValue(transferPackage.value);

        } else {

            return new InvalidValue("Error:BAD_NUMBER");
        }

    }

    @Override
    public Double valueAsDouble() {

        return Double.parseDouble(this.rawValue);

    }

    @Override
    public String valueAsString() {

        return this.rawValue;

    }

}
