package ar.fiuba.tdd.tp1;

public class CreateWorkSheetAction extends Action {

    Workbook workbook;
    String sheetName;

    public CreateWorkSheetAction(Workbook workbookForDoIt, String sheetName, RecordsManager recordsManager) {

        this.recordsManager = recordsManager;

        this.workbook = workbookForDoIt;
        this.sheetName = sheetName;

    }


    @Override
    public void execute() {

        this.workbook.createNewSheet(sheetName); // ¡THE ACTION!

        Sheet sheetCreated = this.workbook.getSheet(sheetName);

        RecordCreateWorkSheet record = new RecordCreateWorkSheet(sheetCreated);
        recordsManager.register(record, workbook);

    }
}
