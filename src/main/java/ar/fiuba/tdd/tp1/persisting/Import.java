package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;

/**
 * Created by fedevm on 18/10/15.
 */
public interface Import {

    public void importFile(String fileName, WorkspacesManager<Workbook> workbooks, String bookName, String sheetName);
}
