package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.CellInterface;
import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Workbook;
import com.owlike.genson.Genson;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class JsonPersistence implements Persistence {

    String fileExtension = "jss";
    Map<String, Object> jsonPersist = new HashMap<>();
    double versionInit = 0.1;

    public JsonPersistence() {
        jsonPersist.clear();
        jsonPersist.put("version", String.valueOf(versionInit));
        List<CellJsonObject> cells = new LinkedList<>();
        jsonPersist.put("cells", cells);
    }


    @Override
    public void persistWorkbook(Workbook workbook, String outName) {
        List<String> sheetNames = workbook.sheetsNames();
        jsonPersist.put("name", workbook.getName());
        for (String sheetName : sheetNames) {
            accumulateCellsFromSheet(workbook.getSheet(sheetName));
        }
        PrintWriter fout = null;
        try {
            fout = new PrintWriter(outNameWithExtension(outName), "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Genson jsonPersister = new Genson();
        jsonPersister.serialize(jsonPersist, fout);
    }

    @Override
    public void persistSheet(Workbook workbook, String sheetName, String outName) {

    }

    private void accumulateCellsFromSheet(Sheet thisSheet) {
        List<CellInterface> cellsToAccumulate = thisSheet.getCellsOrdered();
        for (CellInterface cell : cellsToAccumulate) {
            CellJsonObject newCellJson = new CellJsonObject();
            cell.setJsonPersistant(newCellJson);
            ((List<CellJsonObject>) (jsonPersist.get("cells"))).add(newCellJson);
        }
    }

    private String outNameWithExtension(String outName) {
        if (!outName.matches("[^\\.]*\\.[^\\.]*")) {
            return outName + "." + fileExtension;
        }
        return outName;
    }
}
