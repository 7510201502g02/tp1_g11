package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Workbook;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public interface Persistence {


    void persistWorkbook(Workbook workbook, String outName) throws FileNotFoundException, UnsupportedEncodingException;

    void persistSheet(Workbook workbook, String sheetName, String outName);

}
