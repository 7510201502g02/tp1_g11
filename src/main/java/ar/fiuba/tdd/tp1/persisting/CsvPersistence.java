package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.*;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;


public class CsvPersistence implements Persistence {

    int fileRow = 1;
    int fileColumn = 1;

    @Override
    public void persistWorkbook(Workbook workbook, String outName) {

        List<String> sheetNames = workbook.sheetsNames();
        for (String name : sheetNames) {
            persistSheet(workbook, name, outName);
        }
    }

    @Override
    public void persistSheet(Workbook workbook, String sheetName, String outName) {
        Sheet sheet = workbook.getSheet(sheetName);
        if (outName.isEmpty()) {
            outName = workbook.getName() + "-" + sheet.getName() + ".csv";
        } else if (!outName.matches("[a-zA-Z0-9]+\\.csv$")) {
            outName = outName + ".csv";
        }
        persistThisSheet(sheet, outName);
    }

    private void resetCounter() {
        fileRow = 1;
        fileColumn = 1;
    }

    private void persistThisSheet(Sheet sheetToPersist, String outName) {
        this.resetCounter();
        PrintWriter fout = null;
        try {
            fout = new PrintWriter(outName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (fout != null) {
            List<CellInterface> cellsToPersist = sheetToPersist.getCellsOrdered();
            for (CellInterface cellInterface : cellsToPersist) {
                toOutput(cellInterface.getPosition(), cellInterface.getValue(), fout);
            }
            fout.close();
        }
    }

    private void toOutput(Position pos, Value value, PrintWriter fout) {
        String jumps = getJumps(pos);
        if (!jumps.isEmpty()) {
            fout.print(jumps);
            fileColumn = 1;
        }
        String separator = getSeparators(pos);
        fout.print(separator + value.valueAsString());
    }

    private String getJumps(Position pos) {
        String jumps = "";
        while (fileRow < Integer.parseInt(pos.getRow())) {
            jumps = jumps.concat("\n");
            fileRow++;
        }
        return jumps;
    }

    private String getSeparators(Position pos) {
        String separator = "";
        while (fileColumn < mapletterToColumn(pos.getColumn())) {
            separator = separator.concat(",");
            fileColumn++;
        }
        return separator;
    }

    private int mapletterToColumn(String letter) {
        int column = 0;
        int length = letter.length();
        for (int i = 0; i < length; i++) {
            column += (letter.charAt(i) - 64) * Math.pow(26, length - i - 1);
        }
        return column;
    }
}
