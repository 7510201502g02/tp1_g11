package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;
import com.owlike.genson.Genson;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;
import java.util.NoSuchElementException;

public class JsonImport implements Import {

    // sheetname en este caso no se usa!
    @Override
    public void importFile(String fileName, WorkspacesManager<Workbook> workbooks, String bookName, String sheetName) {
        InputStreamReader fileRead = null;
        try {
            fileRead = new InputStreamReader(new FileInputStream(fileName), "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Genson jsonParser = new Genson();
        Map<String, Object> jsonImport = jsonParser.deserialize(fileRead, Map.class);
        processJson(jsonImport, workbooks);
    }

    private void processJson(Map<String, Object> json, WorkspacesManager<Workbook> workbooks) {

        Workbook workbook;

        /*
            Asi carga la nueva, pero si ya existe workbook
            con ese nombre, te la caga pisando
        */
        String workbookName = String.valueOf(json.get("name"));

        try {
            workbooks.remove(workbookName);
        } catch (NoSuchElementException e) {
            // do nothing
        }

        workbooks.create(workbookName);
        workbook = workbooks.get(workbookName);

        /*
            asi solapa, carga los nuevos valores pisando los viejos.
            Pero los preexistentes, que no existan en la cargada
            los mantiene.
        */
        //String workbookName = String.valueOf(json.get("name")); lo hago villa para que no lo tome el cpd =(
        /*try {

            workbook = workbooks.get( String.valueOf(json.get("name")) );

        } catch (NoSuchElementException e) {

            workbooks.create( String.valueOf(json.get("name")) );
            workbook = workbooks.get( String.valueOf(json.get("name")) );
        }*/

        ArrayList<Map<String, Object>> cells = (ArrayList<Map<String, Object>>) json.get("cells");
        for (Map<String, Object> cell : cells) {
            processCellJson(cell, workbook);
        }
    }

    private void processCellJson(Map<String, Object> cell, Workbook workbook) {
        Sheet workingSheet;
        String sheetName = String.valueOf(cell.get("sheet"));
        try {
            workingSheet = workbook.getSheet(sheetName);
        } catch (NoSuchElementException ex) {
            workbook.createNewSheet(sheetName);
            workingSheet = workbook.getSheet(sheetName);
        }

        workingSheet.setCellValue(String.valueOf(cell.get("id")), String.valueOf(cell.get("value")));
        workingSheet.setValueTypeTo(String.valueOf(cell.get("id")), String.valueOf(cell.get("valueType")));

        if (!((Map<String, Object>) cell.get("formatter")).isEmpty()) {
            setFormatToCell(cell, workingSheet);
        }
    }

    private void setFormatToCell(Map<String, Object> cell, Sheet sheet) {
        for (String format : ((Map<String, Object>) cell.get("formatter")).keySet()) {
            String[] temp = format.split("\\.");
            String formatting = temp[0];
            String formato = String.valueOf(((Map<String, Object>) cell.get("formatter")).get(format));
            sheet.setFormatTo(String.valueOf(cell.get("id")), formatting, formato);
        }

    }
}
