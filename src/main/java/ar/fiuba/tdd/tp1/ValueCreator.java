package ar.fiuba.tdd.tp1;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ValueCreator extends Creator {

    LinkedList<Value> supportedValues = new LinkedList<>();

    public ValueCreator() {

        /*
        importa el orden, text va agarrar cualquier cosa
            que sea string y crear jaaa
         */
        supportedValues.addAll(Arrays
                .asList(new NumberValue(),
                        new FormulaValue(),
                        new DateValue(),
                        new TextValue()));
    }

    @Override
    public List<Value> getSupportedCreateElements() {

        return this.supportedValues;

    }

    @Override
    public Value getCreateInvalidElement() {

        return new InvalidValue();

    }

}
