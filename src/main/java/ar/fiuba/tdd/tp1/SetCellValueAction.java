package ar.fiuba.tdd.tp1;


import java.util.NoSuchElementException;

public class SetCellValueAction extends Action {

    Sheet sheet;
    String cellPosition;
    String value;

    public SetCellValueAction(Sheet sheetForDoIt, String cellPosition, String value, RecordsManager recordsManager) {

        this.recordsManager = recordsManager;

        this.sheet = sheetForDoIt;
        this.cellPosition = cellPosition;
        this.value = value;

    }

    public void execute() {

        Value oldValue;
        Boolean oldCellHasValue;

        // la pachorra de modularizar este codigo que necesito un metodo que devuelva dos valores.
        try {

            oldValue = this.sheet.getCell(cellPosition).getValue();
            oldCellHasValue = true;

        } catch (NoSuchElementException e) {

            oldValue = null;
            oldCellHasValue = false;

        }

        this.sheet.setCellValue(cellPosition, value); // ¡THE ACTION!
        Value newValue = this.sheet.getCell(cellPosition).getValue();

        RecordSetCellValue record = new RecordSetCellValue(cellPosition, oldValue, newValue, oldCellHasValue);
        recordsManager.register(record, sheet);

    }
}
