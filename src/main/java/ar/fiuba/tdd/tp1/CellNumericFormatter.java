package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaExpressionException;
import ar.fiuba.tdd.tp1.exceptions.InvalidValueException;

public abstract class CellNumericFormatter extends CellFormatter {

    @Override
    public String valueAsString() {

        if (isANumber()) {

            return applyFormat();

        } else {

            return cell.valueAsString();
        }

    }

    // el peor codigo jajaja
    protected boolean isANumber() {

        try {

            cell.valueAsDouble();
            return true;

        } catch (InvalidFormulaExpressionException e) {

            return false;

        } catch (InvalidValueException e) {

            return false;
        }

    }

    protected abstract String applyFormat();
}
