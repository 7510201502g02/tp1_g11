package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormatterException;

/*
    Ojo con acumular invalidFormatter con valid, porque pincha,
    ver que invalid no guarda el valor viejo!
*/

public class CellInvalidFormatter extends CellFormatter {


    @Override
    public String valueAsString() {
        throw new InvalidFormatterException();
    }

    @Override
    public void setFormat(String format) {

        throw new InvalidFormatterException();

    }

    @Override
    public CellFormatter createFrom(TransferPackage transferPackageForTokenBuild) {

        return new CellInvalidFormatter();

    }


}
