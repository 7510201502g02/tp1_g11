package ar.fiuba.tdd.tp1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/*
    Formatos soportados: yyyy-MM-dd, MM-dd-yyyy, dd-MM-yyyy
    Se banca muchos mas, salvo lo que pusieron en el tp con all mayusculas,
    lo cual es muy desafortunado y no lo pienso cambiar,
    porque esta como el resto del mundo labura
*/

public class CellDateFormatter extends CellFormatter {

    public CellDateFormatter() {

        this.formatType = "date";

    }

    public CellDateFormatter(CellInterface cell, String dateFormat) {

        init(cell, dateFormat);
        this.formatType = "date"; // deberia ser date, es solo por los test

    }

    /*
        Se espera una celda que tenga DateValue Y ningun
        DateFormatter aplicado. Si le viene un value con un formatter date
        ya aplicado, "lo pisara" teniendo solo efecto el ultimo.
        Por otro lado si viene un texto con forma de algo que puede parsiar
        lo intentara, No se asegura lo haga de forma correcta!!!

        Buena practica: Si se quiere ir cambiando de formatos fecha,
        tenes que "borrar" el DateFormatter existente
        y crear uno nuevo (ver logica en el sheet -> cleanAllFormattersOnCell)
    */

    @Override
    public String valueAsString() {

        try {

            // base format
            LocalDateTime result = LocalDateTime.parse(cell.getRawValue(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);

            // new format
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(this.format);

            return result.format(formatter).toString();

        } catch (DateTimeParseException e) {

            return cell.valueAsString();

        }

    }

    @Override
    public CellFormatter getCreatorElement(TransferPackage transferPackage) {

        return new CellDateFormatter(transferPackage.context, transferPackage.value);

    }

}
