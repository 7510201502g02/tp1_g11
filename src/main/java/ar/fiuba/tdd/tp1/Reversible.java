package ar.fiuba.tdd.tp1;

public interface Reversible {

    default void undo(Record record) {

        record.undo(this);

    }

    default void redo(Record record) {

        record.redo(this);

    }
}
