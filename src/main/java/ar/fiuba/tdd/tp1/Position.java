package ar.fiuba.tdd.tp1;

import java.util.StringTokenizer;

public class Position {

    String sheetPosition = null;
    String column;
    String row;

    // todo: ni ahi se establece un orden de posiciones

    public Position(String position) { // sheetName.AB33 o AB33

        if (isValid(position)) {

            StringTokenizer positionTokenize = new StringTokenizer(position, "."); // [sheetName , AB33]

            if (hasSheetName(positionTokenize)) {

                createPositionWithSheetReference(positionTokenize);

            } else {

                createPosition(position);
            }
        }


    }

    public static boolean isValid(String position) {

        String onlyCellRegularExpression = "([a-zA-Z0-9\\s]+\\.)?[A-Z]+[\\d]+";
        return position.matches(onlyCellRegularExpression);

    }

    private boolean hasSheetName(StringTokenizer positionTokenize) {

        return positionTokenize.countTokens() == 2;
    }

    private void createPositionWithSheetReference(StringTokenizer positionTokenize) {

        this.sheetPosition = positionTokenize.nextToken();
        createPosition(positionTokenize.nextToken());

    }

    private void createPosition(String simplePosition) { // AB34 o AB3 o A2

        this.column = simplePosition.split("[0-9]")[0];

        String[] rowPosition = simplePosition.split("[A-Z]");
        this.row = rowPosition[rowPosition.length - 1];

    }

    public void setSheetPosition(String newSheetPosition) {
        this.sheetPosition = newSheetPosition;
    }

    public String getSheetPosition() {
        return sheetPosition;
    }

    public String getColumn() {
        return column;
    }

    public String getRow() {
        return row;
    }

    public boolean samePosition(Position otherPosition) {

        return (this.sheetPosition.equals(otherPosition.getSheetPosition())
                && this.column.equals(otherPosition.getColumn())
                && this.row.equals(otherPosition.getRow()));

    }

    @Override
    public String toString() {

        return sheetPosition + "." + column + row;

    }
}
