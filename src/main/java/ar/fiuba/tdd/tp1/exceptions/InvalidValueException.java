package ar.fiuba.tdd.tp1.exceptions;

public class InvalidValueException extends RuntimeException {

    public InvalidValueException(String message) {

        super(message);

    }

}
