package ar.fiuba.tdd.tp1.exceptions;

public class HasNotScannerSymbol extends RuntimeException {

    @Override
    public String getMessage() {
        return "HasNotScannerSymbol";
    }
}
