package ar.fiuba.tdd.tp1.exceptions;


public class CreateWorkSpaceException extends RuntimeException {

    public CreateWorkSpaceException(String message) {

        super(message);

    }
}
