package ar.fiuba.tdd.tp1.exceptions;


public class InvalidFormatterException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Invalid Formatter";
    }

}