package ar.fiuba.tdd.tp1.exceptions;

public class InvalidFormulaExpressionException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Error:BAD_FORMULA";
    }
}
