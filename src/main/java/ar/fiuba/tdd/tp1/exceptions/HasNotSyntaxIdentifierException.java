package ar.fiuba.tdd.tp1.exceptions;

public class HasNotSyntaxIdentifierException extends RuntimeException {

    @Override
    public String getMessage() {
        return "HasNotSyntaxIdentifierException";
    }
}
