package ar.fiuba.tdd.tp1.exceptions;

public class HasNotSyntaxRuleException extends RuntimeException {

    @Override
    public String getMessage() {
        return "HasNotSyntaxRuleException";
    }
}
