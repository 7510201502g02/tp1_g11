package ar.fiuba.tdd.tp1.exceptions;


public class NoRecordsRegistered extends RuntimeException {

    public NoRecordsRegistered(String message) {

        super(message);

    }
}
