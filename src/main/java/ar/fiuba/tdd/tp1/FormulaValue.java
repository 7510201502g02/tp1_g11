package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.tokens.ExpressionSolver;
import ar.fiuba.tdd.tp1.tokens.Scanner;
import ar.fiuba.tdd.tp1.tokens.SyntacticAnalizer;
import ar.fiuba.tdd.tp1.tokens.TokensExpression;

import java.util.List;

public class FormulaValue extends Value {

    private ExpressionSolver expressionSolver = new ExpressionSolver();
    private TokensExpression tokensExpression;
    private TransferPackage transferPackage;

    private void init(String rawValue) {
        this.valueType = "formula";
        this.rawValue = rawValue;
    }

    public FormulaValue() {
        init(null);
    }

    private FormulaValue(TransferPackage transferPackage) {

        this.transferPackage = transferPackage;

        init(this.transferPackage.value);

        transferPackage.value = transferPackage.value.substring(1);
        this.tokensExpression = new Scanner().scan(transferPackage);

    }

    @Override
    public boolean internalValid(TransferPackage transferPackage) {

        return transferPackage.value.startsWith("=");

    }

    @Override
    public Value getCreatorElement(TransferPackage transferPackage) {

        if (internalValid(transferPackage)) {

            return new FormulaValue(transferPackage);

        } else {

            return new InvalidValue("Error:BAD_FORMULA");
        }

    }

    @Override
    public Double valueAsDouble() {

        return Double.parseDouble(valueAsString());

    }

    @Override
    public String valueAsString() {

        this.tokensExpression = new Scanner().scan(this.transferPackage);

        if (new SyntacticAnalizer().isValidSyntax(tokensExpression)) {

            return expressionSolver.solveExpression(tokensExpression);

        } else {

            return new InvalidValue("Error:BAD_SYNTAX_FORMULA").valueAsString();
        }

    }

    public List<Position> getCellsReferencesPositions() {

        this.tokensExpression = new Scanner().scan(this.transferPackage);
        return tokensExpression.getCellsReferencesPositions();

    }

}
