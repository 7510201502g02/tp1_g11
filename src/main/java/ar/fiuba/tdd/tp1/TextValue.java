package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.exceptions.InvalidValueException;

public class TextValue extends Value {

    private void init(String rawValue) {
        this.valueType = "text";
        this.rawValue = rawValue;
    }

    public TextValue() {
        init(null);
    }

    private TextValue(String value) {

        init(value);

    }

    @Override
    public boolean internalValid(TransferPackage transferPackage) {

        return true;

    }

    @Override
    public Value getCreatorElement(TransferPackage transferPackage) {

        return new TextValue(transferPackage.value);

    }

    @Override
    public Double valueAsDouble() {
        throw new InvalidValueException("I am text");
    }

    @Override
    public String valueAsString() {
        return this.rawValue;
    }

}
