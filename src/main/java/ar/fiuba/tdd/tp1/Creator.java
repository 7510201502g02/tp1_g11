package ar.fiuba.tdd.tp1;

import java.util.Iterator;
import java.util.List;

public abstract class Creator<T extends CreatorElement> {

    public <T extends CreatorElement> T createFrom(TransferPackage packageToBuildValue) {

        T newElement = getCreateInvalidElement();

        Iterator<? extends CreatorElement> valueSupportedIterator = getSupportedCreateElements().iterator();

        while (!packageToBuildValue.isCreated() && valueSupportedIterator.hasNext()) {

            newElement = valueSupportedIterator.next().createFrom(packageToBuildValue);
        }

        return newElement;
    }

    public abstract List<? extends CreatorElement> getSupportedCreateElements();

    public abstract <T extends CreatorElement> T getCreateInvalidElement();

}




