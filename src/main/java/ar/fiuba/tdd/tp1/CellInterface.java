package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.persisting.CellJsonObject;

import java.util.List;

public interface CellInterface {

    void setValueSince(String value);

    Position getPosition();

    void setCellReferenceManager(CellsReferenceManager cellReferenceManager);

    CellsReferenceManager getCellReferenceManager();

    Double valueAsDouble();

    String valueAsString();

    void setValueType(String type);

    String getRawValue();

    // agregados por para el acction

    Value getValue();

    void setValue(Value value);

    List<Position> getCellsReferencesPositions();

    // agregado para persistencia

    void setJsonPersistant(CellJsonObject jsonObject);

}
