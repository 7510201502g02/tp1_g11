package ar.fiuba.tdd.tp1;


public interface Record {

    void undo(Reversible reversible);

    void redo(Reversible reversible);
}
