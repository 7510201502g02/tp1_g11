package ar.fiuba.tdd.tp1;


public class TransferPackage {

    public String valueType;
    public String value;
    public CellInterface context;
    private Boolean created;

    public TransferPackage(String value, CellInterface context) {

        this.value = value;
        this.context = context;
        this.created = false;
        this.valueType = null;
    }


    public TransferPackage(String value, String valueType, CellInterface context) {

        this.value = value;
        this.valueType = valueType;
        this.context = context;
        this.created = false;

    }

    public void created() {

        this.created = true;

    }

    public boolean isCreated() {

        return created;

    }
}
