package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.BadRangeException;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Leandro on 08-Nov-15. :)
 */
public class AliasManager {
    private static AliasManager ourInstance = new AliasManager();

    private Map<String, String> aliases = new HashMap<>();

    public static AliasManager getInstance() {
        return ourInstance;
    }

    private AliasManager() {
    }

    public void saveRangeAlias(String range, String alias) throws BadRangeException {
        try {
            String[] limitCells = range.split(Pattern.quote(":"));
            if (Position.isValid(limitCells[0]) && Position.isValid(limitCells[1])) {
                aliases.put(alias, range);
            }

        } catch (Exception e) {
            throw new BadRangeException();
        }

    }

    public String getElementByAlias(String alias) {
        return aliases.get(alias);
    }

    public boolean hasAlias(String alias) {
        return aliases.containsKey(alias);
    }
}
