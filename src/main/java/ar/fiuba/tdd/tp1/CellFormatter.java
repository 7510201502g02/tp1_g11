package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.persisting.CellJsonObject;

import java.util.List;

public abstract class CellFormatter implements CellInterface, CreatorElement {

    protected CellInterface cell;
    protected String formatType;
    protected String format;

    @Override
    public void setValueSince(String value) {

        cell.setValueSince(value);

    }

    @Override
    public Position getPosition() {

        return cell.getPosition();

    }

    @Override
    public void setCellReferenceManager(CellsReferenceManager cellReferenceManager) {

        cell.setCellReferenceManager(cellReferenceManager);

    }

    @Override
    public CellsReferenceManager getCellReferenceManager() {

        return cell.getCellReferenceManager();

    }

    @Override
    public Double valueAsDouble() {

        return cell.valueAsDouble();

    }

    // es el que cada formato debera redefinir
    @Override
    public String valueAsString() {

        return cell.valueAsString();

    }

    @Override
    public Value getValue() {

        return cell.getValue();

    }

    @Override
    public void setValue(Value value) {

        cell.setValue(value);

    }


    public void setFormat(String format) {

        this.format = format;

    }

    protected void init(CellInterface cell, String format) {

        this.cell = cell;
        this.format = format;

    }


    @Override
    public List<Position> getCellsReferencesPositions() {

        return cell.getCellsReferencesPositions();

    }

    @Override
    public boolean valid(TransferPackage transferPackage) {

        return formatType.equals(transferPackage.valueType);

    }

    @Override
    public CellFormatter getCreatorInvalidElement() {

        return new CellInvalidFormatter();

    }

    @Override
    public void setJsonPersistant(CellJsonObject jsonObject) {

        String pos = cell.getPosition().getColumn() + cell.getPosition().getRow();
        Value value = cell.getValue();

        jsonObject.setCellBasicValues(cell.getPosition().getSheetPosition(), pos, value.getRawValue(), value.getValueType());
        jsonObject.setCellFormat(formatType, format);
    }

    @Override
    public void setValueType(String type) {

        cell.setValueType(type);

    }

    @Override
    public String getRawValue() {

        return this.cell.getRawValue();

    }


}
