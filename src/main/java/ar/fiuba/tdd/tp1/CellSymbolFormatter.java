package ar.fiuba.tdd.tp1;

public class CellSymbolFormatter extends CellNumericFormatter {

    public CellSymbolFormatter() {

        this.formatType = "symbol";

    }

    /*
        Por como esta implementada le podes poner cualquier symbolo,
        no solo una moneda.
    */

    public CellSymbolFormatter(CellInterface cell, String currencySymbol) {

        init(cell, currencySymbol);
        this.formatType = "symbol";
    }

    @Override
    public String applyFormat() {

        return this.format + " " + cell.valueAsString();

    }

    @Override
    public CellFormatter getCreatorElement(TransferPackage transferPackage) {


        return new CellSymbolFormatter(transferPackage.context, transferPackage.value);

    }

}
