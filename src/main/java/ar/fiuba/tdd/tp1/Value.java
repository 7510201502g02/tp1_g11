package ar.fiuba.tdd.tp1;

import java.util.LinkedList;
import java.util.List;

public abstract class Value implements CreatorElement {

    protected String valueType;
    protected String rawValue;

    public abstract Double valueAsDouble();

    public abstract String valueAsString();


    @Override
    public boolean valid(TransferPackage transferPackage) {

        if (transferPackage.valueType.equals("undefined")) {

            return internalValid(transferPackage); // inferred logic

        } else {

            if (valueType.equals(transferPackage.valueType)) {

                return true;

            } else {

                return false;

            }
        }
    }

    protected abstract boolean internalValid(TransferPackage transferPackage);

    public List<Position> getCellsReferencesPositions() {

        return new LinkedList<>();

    }

    public Value getCreatorInvalidElement() {

        return new InvalidValue();

    }

    // para soportar el setType

    public String getRawValue() {

        return rawValue;

    }

    public String getValueType() {

        return valueType;

    }
}
