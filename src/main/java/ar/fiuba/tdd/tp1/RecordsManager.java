package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.NoRecordsRegistered;

import java.util.LinkedList;
import java.util.NoSuchElementException;


/*
    Hasta ahora es un buffer bobo, es decir, salta sobre cualquier "contexto".
    Si queres que no lo haga, habria que a los reversibles asignarles algun tipo de
    prioridad o nivel, y armar una logica de como salta, ademas de permitirle
    al usuario poder saltar de nivel. Es decir, cuando estas posado sobre una sheet
    no queres hacer "undo" de otra, y si cambias de woorksheet tenes que saltar de "contexto"
 */

public class RecordsManager {

    private int actualIndex = -1;

    // Por que java no tiene tuplas copadas, o por lo menos que mk conozca.
    private LinkedList<Record> records = new LinkedList<>();
    private LinkedList<Reversible> reversibles = new LinkedList<>();

    /*
        ojo el orden del index no da lo mismo, me lo rre morfe la primera vez jeee
     */

    public void undo() {

        try {

            reversibles.get(actualIndex).undo(records.get(actualIndex));
            actualIndex--;
            updateIndex();

        } catch (NoSuchElementException e) {

            throw new NoRecordsRegistered("Error: Don't had any acction for undo");

        }


    }

    public void redo() {

        try {

            actualIndex++;
            updateIndex();
            reversibles.get(actualIndex).redo(records.get(actualIndex));

        } catch (NoSuchElementException e) {

            throw new NoRecordsRegistered("Error: Don't had any acction for redo");

        }

    }

    public void register(Record record, Reversible reversible) {

        records.add(record);
        reversibles.add(reversible);
        actualIndex++;

    }

    private void updateIndex() {

        if (actualIndex < -1) {

            actualIndex++;

        } else if (actualIndex == records.size()) {

            actualIndex--;

        }

    }
}
