package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.exceptions.CreateWorkSpaceException;

public class RecordCreateWorkSheet implements Record {

    Sheet sheetCreated;

    public RecordCreateWorkSheet(Sheet sheetCreated) {

        this.sheetCreated = sheetCreated;

    }

    @Override
    public void undo(Reversible reversible) {

        ((Workbook) reversible).removeSheet(sheetCreated.getName());

    }

    @Override
    public void redo(Reversible reversible) {

        try {

            ((Workbook) reversible).addSheet(sheetCreated);

        } catch (CreateWorkSpaceException e) {

            // do nothing
        }

    }
}
