package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.exceptions.InvalidValueException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChangeCellValueTypeTest {

    CellInterface cell;
    private static final double DELTA = 0.0001;

    @Test(expected = InvalidValueException.class)
    public void changeNumberToText() {

        cell = new Cell(null, "5", null);
        assertEquals(5, cell.valueAsDouble(), DELTA);

        cell.setValueType("text");

        cell.valueAsDouble();

    }

    @Test
    public void changeNumberToTextAndReturnToNumber() {

        cell = new Cell(null, "5", null);
        assertEquals(5, cell.valueAsDouble(), DELTA);

        cell.setValueType("text");
        cell.setValueType("number");

        assertEquals(5, cell.valueAsDouble(), DELTA);

    }

    /*
        como el texto guarda un "string" valido para
        el formatter este se puede aplicar.
        -> Hacer o jugar con esto no es buena practica
    */

    /*@Test
    public void changeDateWithFormatterToText() {
        cell = new Cell(null,"2012-07-14T00:00:00Z",null);
        assertEquals("2012-07-14T00:00:00Z", cell.valueAsString());

        cell = new CellDateFormatter(cell,"MM-dd-yyyy");

        assertEquals("07-14-2012", cell.valueAsString());
        cell.setValueType("text");
        assertEquals("07-14-2012", cell.valueAsString());

    }*/

    @Test()
    public void changeTextToDate() {

        cell = new Cell(null, "hola", null);
        assertEquals("hola", cell.valueAsString());

        cell.setValueType("date");

        assertEquals("Error:BAD_DATE", cell.valueAsString());
    }

    @Test()
    public void changeDateToNumber() {

        cell = new Cell(null, "2012-07-14T00:00:00Z", null);
        assertEquals("2012-07-14T00:00:00Z", cell.valueAsString());

        cell.setValueType("number");

        cell.valueAsString();
        assertEquals("Error:BAD_NUMBER", cell.valueAsString());

    }
}
