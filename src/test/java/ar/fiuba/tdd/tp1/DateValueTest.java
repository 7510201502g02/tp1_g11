package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InvalidValueException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DateValueTest {

    DateValue fakeFactory;

    @Before
    public void setUp() {

        fakeFactory = new DateValue();

    }

    @Test
    public void createDateValueTest() {

        TransferPackage transferPackage = new TransferPackage("2012-07-14T00:00:00Z", "undefined", null);
        Value dateValue = fakeFactory.createFrom(transferPackage);
        assertEquals("2012-07-14T00:00:00Z", dateValue.valueAsString());
    }

    @Test()
    public void invalidFormatForDateValueTest() {

        TransferPackage transferPackage = new TransferPackage("17/10/2015", "undefined", null);
        Value dateValue = fakeFactory.createFrom(transferPackage);

        assertEquals(new InvalidValueException("invalid value").getMessage(), dateValue.valueAsString());
    }


}
