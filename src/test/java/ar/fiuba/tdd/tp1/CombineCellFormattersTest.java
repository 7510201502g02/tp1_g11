package ar.fiuba.tdd.tp1;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CombineCellFormattersTest {


    CellInterface cell;
    CellFormatter cellFormatter;

    @Test
    public void onlyCanApplyOneCellDecimalsFormatterTest() {

        // respeta solo el ultimo agregado

        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellFormatter = new CellDecimalsFormatter(cell, "0");
        cellFormatter = new CellDecimalsFormatter(cellFormatter, "4");

        assertEquals("20.0000", cellFormatter.valueAsString());
        assertEquals("20.123456", cellFormatter.getRawValue());

    }

    @Test
    public void linkBetweenDecimalsAndSymbolFormatterTest() {

        // combina los dos formatos no importa el orden

        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellFormatter = new CellDecimalsFormatter(cell, "3");

        assertEquals("20.123", cellFormatter.valueAsString());

        cellFormatter = new CellSymbolFormatter(cellFormatter, "$");

        assertEquals("$ 20.123", cellFormatter.valueAsString());

    }

    @Test
    public void linkBetweenSymbolAndDecimalsFormatterTest() {

        // combina los dos formatos no importa el orden

        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellFormatter = new CellSymbolFormatter(cell, "$");

        assertEquals("$ 20.123456", cellFormatter.valueAsString());

        cellFormatter = new CellDecimalsFormatter(cellFormatter, "3");

        assertEquals("$ 20.123", cellFormatter.valueAsString());

    }

    @Test
    public void linkBetweenDecimalsAndDateFormatterTest() {

        // respeta solo el ultimo agregado

        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellFormatter = new CellDecimalsFormatter(cell, "3");

        assertEquals("20.123", cellFormatter.valueAsString());

        cellFormatter = new CellDateFormatter(cellFormatter, "dd-MM-yyyy");

        assertEquals("20.123", cellFormatter.valueAsString());

        cellFormatter.setValueSince("2015-10-17T00:00:00Z");

        assertEquals("2015-10-17T00:00:00Z", cell.valueAsString());
        assertEquals("17-10-2015", cellFormatter.valueAsString());

    }


    @Test
    public void linkBetweenDateAndSymbolFormatterTest() {

        // lo deja en pasivo al que corresponda

        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellFormatter = new CellDateFormatter(cell, "dd-MM-yyyy");

        assertEquals("20.123456", cell.valueAsString());

        cellFormatter = new CellSymbolFormatter(cellFormatter, "US$");

        assertEquals("US$ 20.123456", cellFormatter.valueAsString());

        cell.setValueSince("2015-10-18T00:00:00Z");

        assertEquals("2015-10-18T00:00:00Z", cell.valueAsString());
        assertEquals("18-10-2015", cellFormatter.valueAsString());

    }

    @Test
    public void onlyCanApplyOneCellDateFormatterTest() {

        // respeta solo el ultimo agregado

        cell = new Cell(null, "2015-10-17T00:00:00Z", null);

        cellFormatter = new CellDateFormatter(cell, "MM-dd-yyyy");

        assertEquals("10-17-2015", cellFormatter.valueAsString());

        CellDateFormatter newCellDateFormatter = new CellDateFormatter(cellFormatter, "dd-MM-yyyy");

        assertEquals("17-10-2015", newCellDateFormatter.valueAsString());

    }

    @Test
    public void linkBetweenSymbolAndSymbolFormatterTest() {

        // va encadenando formatos symbol

        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellFormatter = new CellSymbolFormatter(cell, "$");

        assertEquals("$ 20.123456", cellFormatter.valueAsString());

        cellFormatter = new CellSymbolFormatter(cellFormatter, "#");

        assertEquals("# $ 20.123456", cellFormatter.valueAsString());

    }

}
