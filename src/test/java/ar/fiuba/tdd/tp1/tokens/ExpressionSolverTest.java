package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class ExpressionSolverTest {

    private ExpressionSolver expressionSolver = new ExpressionSolver();

    @Test
    public void solveExpression() {

        String expression = "3*(4+2*4-(3*2))-4";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 14, 0.01);

    }

    @Test
    public void solveExpressionWithFunction() {

        String expression = "3*(4+2*4-SUM(4,2+4,6))-4";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), -16, 0.01);

    }

    @Test
    public void solveExpressionWithStrings() {

        String expression = "CONCAT('hola',SUM(4,2),'chau')";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));

        assertEquals(expressionSolver.solveExpression(tokensExpression), "hola6chau");
    }

    @Test
    public void solveWithNegativeNumbers() {

        String expression = "1 + 1 - (-8) + 4";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 14, 0.01);

    }

    @Test
    public void solveSubtractionFunction() {

        String expression = "SUBTRACTION(4, 3.62)";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 0.38, 0.01);
    }

    @Test
    public void testSolvePowFunction() {
        final String expression = "2 ^ 2";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 4, 0.01);
    }


    @Test
    public void testSolvePowFunctionWithNegativePow() {
        final String expression = "2 ^ (-1)";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals(0.5, Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 0.01);
    }

    @Test
    public void testSolvePowFunctionWithNegativeBase() {
        final String expression = "(-2) ^ 3";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals(-8, Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 0.01);
    }

    @Test
    public void testSolveLeft() {
        final String expression = "LEFT( 'hola' , 2 )";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals("ho", expressionSolver.solveExpression(tokensExpression));
    }

    @Test
    public void testSolveLeftEndOutOfRangeReturnsTheWholeExpression() {
        final String expression = "LEFT( 'hola' , 22 )";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals("hola", expressionSolver.solveExpression(tokensExpression));
    }

    @Test
    public void testSolveRight() {
        final String expression = "RIGHT( 'hola' , 2 )";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals("la", expressionSolver.solveExpression(tokensExpression));
    }

    @Test
    public void testSolveRightEndOutOfRangeReturnsTheWholeExpression() {
        final String expression = "RIGHT( 'hola' , 22 )";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals("hola", expressionSolver.solveExpression(tokensExpression));
    }

    @Test
    public void testSolvePrintf() {
        final String expression = "PRINTF( 'hola$0' , 'hola!' )";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals("holahola!", expressionSolver.solveExpression(tokensExpression));
    }

    @Test
    public void testSolvePrintfWhitNoExistingArgument() {
        final String expression = "PRINTF( 'hola$0'  )";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals("hola$0", expressionSolver.solveExpression(tokensExpression));
    }

    @Test
    public void testSolvePrintfWhitArgumentOutOfRange() {
        final String expression = "PRINTF( 'hola$9', 'hola!'  )";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        assertEquals("hola$9", expressionSolver.solveExpression(tokensExpression));
    }

    @Test
    public void testSolveToday() {
        final String expression = "TODAY()";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression, null));
        //busco que coincidan en casi todo.
        String cellValue = expressionSolver.solveExpression(tokensExpression);
        assertEquals(LocalDateTime.now().toString().substring(0, LocalDateTime.now().toString().length() - 3),
                cellValue.substring(0, cellValue.length() - 3));
    }
}
