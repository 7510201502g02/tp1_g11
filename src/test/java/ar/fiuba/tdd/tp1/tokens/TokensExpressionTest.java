package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.Creator;
import ar.fiuba.tdd.tp1.TransferPackage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TokensExpressionTest {

    TokensExpression tokensExpression = new TokensExpression();
    Creator tokenFactory = new TokenCreator();

    @Test
    public void createASimpleTokensExpression() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        Token plusToken = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        Token tokenNumeric2 = (Token) tokenFactory.createFrom(new TransferPackage("3", null));

        tokensExpression.addToTheExpression(tokenNumeric1);
        tokensExpression.addToTheExpression(plusToken);
        tokensExpression.addToTheExpression(tokenNumeric2);

        assertEquals("2+3", tokensExpression.toString());

    }

    @Test
    public void getLeftSideOfATokensExpressionWithOnlyOneElement() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        tokensExpression.addToTheExpression(tokenNumeric1);

        assertEquals("", tokensExpression.getLeftSideOfTheExpression(tokenNumeric1).toString());

    }

    @Test
    public void getRightSideOfATokensExpressionWithOnlyOneElement() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        tokensExpression.addToTheExpression(tokenNumeric1);

        assertEquals("", tokensExpression.getRightSideOfTheExpression(tokenNumeric1).toString());

    }

    @Test
    public void getLeftSideOfASimpleTokensExpression() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        Token plusToken = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        Token tokenNumeric2 = (Token) tokenFactory.createFrom(new TransferPackage("3", null));

        tokensExpression.addToTheExpression(tokenNumeric1);
        tokensExpression.addToTheExpression(plusToken);
        tokensExpression.addToTheExpression(tokenNumeric2);

        assertEquals("2", tokensExpression.getLeftSideOfTheExpression(plusToken).toString());

    }

    @Test
    public void getRightSideOfASimpleTokensExpression() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        Token plusToken = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        Token tokenNumeric2 = (Token) tokenFactory.createFrom(new TransferPackage("3", null));

        tokensExpression.addToTheExpression(tokenNumeric1);
        tokensExpression.addToTheExpression(plusToken);
        tokensExpression.addToTheExpression(tokenNumeric2);

        assertEquals("+3", tokensExpression.getRightSideOfTheExpression(tokenNumeric1).toString());

    }

    @Test
    public void createATokensExpressionWithInvalidToken() {

        Token invalidToken = (Token) tokenFactory.createFrom(new TransferPackage("¬", null));
        Token tokenNumeric = (Token) tokenFactory.createFrom(new TransferPackage("3", null));

        tokensExpression.addToTheExpression(invalidToken);
        tokensExpression.addToTheExpression(tokenNumeric);

        assertEquals("null3", tokensExpression.toString());

    }

    @Test // queria probar que no tome los 2 plus como la misma cosa
    public void getRightSideOfASimpleTokensExpressionWithTwoPlusTokens() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        Token plusToken1 = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        Token plusToken2 = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        Token tokenNumeric2 = (Token) tokenFactory.createFrom(new TransferPackage("3", null));

        tokensExpression.addToTheExpression(tokenNumeric1);
        tokensExpression.addToTheExpression(plusToken1);
        tokensExpression.addToTheExpression(plusToken2);
        tokensExpression.addToTheExpression(tokenNumeric2);

        assertEquals("+3", tokensExpression.getRightSideOfTheExpression(plusToken1).toString());

    }


    @Test     // si el token no pertence te devuelve un una TokenExpression vacia
    public void intentToGetLeftSideOfATokensExpressionWithATokenDoesNotBelongToTheExpression() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        Token plusToken1 = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        Token tokenNumeric2 = (Token) tokenFactory.createFrom(new TransferPackage("3", null));

        tokensExpression.addToTheExpression(tokenNumeric1);
        tokensExpression.addToTheExpression(plusToken1);
        tokensExpression.addToTheExpression(tokenNumeric2);

        Token plusTokenDoesNotBelong = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        assertEquals("", tokensExpression.getLeftSideOfTheExpression(plusTokenDoesNotBelong).toString());

    }


    @Test // si el token no pertence te devuelve toda la TokenExpression
    public void intentToGetRightSideOfATokensExpressionWithATokenDoesNotBelongToTheExpression() {

        Token tokenNumeric1 = (Token) tokenFactory.createFrom(new TransferPackage("2", null));
        Token plusToken1 = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        Token tokenNumeric2 = (Token) tokenFactory.createFrom(new TransferPackage("3", null));

        tokensExpression.addToTheExpression(tokenNumeric1);
        tokensExpression.addToTheExpression(plusToken1);
        tokensExpression.addToTheExpression(tokenNumeric2);

        Token plusTokenDoesNotBelong = (Token) tokenFactory.createFrom(new TransferPackage("+", null));
        assertEquals("2+3", tokensExpression.getRightSideOfTheExpression(plusTokenDoesNotBelong).toString());

    }
}
