package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.Creator;
import ar.fiuba.tdd.tp1.TransferPackage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ParenthesisSolverTest {

    private Creator tokenFactory = new TokenCreator();

    @Test
    public void mapParenthesisFromExpression() {

        TokensExpression tkExpression = new TokensExpression();

        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("2", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("+", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("(", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("3", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("-", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("4", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage(")", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("*", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("8", null)));

        ParenthesisSolver solver = new ParenthesisSolver();

        tkExpression = solver.mapParenthesis(tkExpression);

        assertTrue(tkExpression.toString().equals("2+$*8"));

    }

    @Test
    public void mapMultipleParenthesisFromExpression() {

        TokensExpression tkExpression = new TokensExpression();

        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("2", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("+", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("(", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("3", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("-", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("4", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage(")", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("*", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("(", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("8", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("/", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("4", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage(")", null)));

        ParenthesisSolver solver = new ParenthesisSolver();

        tkExpression = solver.mapParenthesis(tkExpression);

        assertTrue(tkExpression.toString().equals("2+$*$"));

    }

    @Test
    public void mapFunctionParenthesis() {

        TokensExpression tkExpression = new TokensExpression();

        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("SUM", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("(", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("4", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("+", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("6", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage(",", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("8", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage(")", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("+", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("8", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("/", null)));
        tkExpression.addToTheExpression((Token) tokenFactory.createFrom(new TransferPackage("4", null)));

        ParenthesisSolver solver = new ParenthesisSolver();

        tkExpression = solver.mapParenthesis(tkExpression);

        assertTrue(tkExpression.toString().equals("SUM$+8/4"));
    }
}
