package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScannerTest {

    Scanner scanner = new Scanner();

    @Test
    public void scanExpressionStringWithComplexNumericToken() {

        String expression = "4.2 * 1 / (4 + 8)";

        TokensExpression tokensExpression = scanner.scan(new TransferPackage(expression, null));

        assertEquals("4.2*1/(4+8)", tokensExpression.toString());

    }

    @Test
    public void scanExpressionWithStrings() {

        String expression = "CONCAT('hola', 'chau')";

        TokensExpression tokensExpression = scanner.scan(new TransferPackage(expression, null));

        assertEquals("CONCAT('hola','chau')", tokensExpression.toString());
    }

    @Test
    public void scanAComplexExpressionString() {

        //String expression = "SUM(A1:A2, 6+5, A3) + 4 / 8)";
        String expression = "SUM(4, 6+5, 3 * 1 ) * (4 + 8)";


        TokensExpression tokensExpression = scanner.scan(new TransferPackage(expression, null));

        /*LinkedList<String> toknes = scanner.tokenize(expression);

        System.out.println("begin scanAComplexExpressionString output");
        for (int i = 0 ; i < toknes.size(); i++) {
            System.out.println(toknes.get(i));
        }
        assert (true);*/

        assertEquals("SUM(4,6+5,3*1)*(4+8)", tokensExpression.toString());

    }

    @Test
    public void adaptExpressionsWithNegativeNumbers() {

        String expression = "3 + 4 / -3";

        assertEquals("3+4/(-1)/3", scanner.adaptExpression(expression));
    }

    /*
        Ojo que el "recognizeSupportedTokens" del Scanner anda medio medio,
        cosas como: "2 ! 3" te lo saca completo [invalidToken], porque obviamente no pone º en
        ningun lado, ya que no pone º alrrededor de los numeros y celdas. Es decir,
        si hay invalidos y numeros juntos, saca un unico invalid token lo cual,
        si despues se pasa el syntaxanalizer y esa expresion quedo de un elemento,
        dira que es valida, lo cual no es cierto,
        igualmente saltara el error en el eval del invalidToken =D
    */

}
