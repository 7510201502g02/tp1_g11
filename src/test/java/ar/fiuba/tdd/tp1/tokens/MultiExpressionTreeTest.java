package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by gonza on 10/2/15.
 */
public class MultiExpressionTreeTest {

    @Test
    public void showTree() {

        //Prueba dummy para mostrar la funcionalidad del arbol. El assert no evalua nada :)

        String expression = "4+SUM(6,4+3,SUM(2+8))";

        Scanner scanner = new Scanner();

        // NO TE VA SERVIR PORQUE PARA EXPRESIONES CON CELDAS TIENE QUE ESTAR EXISTIR EL MECANISMO DE REFERENCIAR
        TokensExpression tkExpression = scanner.scan(new TransferPackage(expression, null));

        ParenthesisSolver parenthesisSolver = new ParenthesisSolver();

        tkExpression = parenthesisSolver.mapParenthesis(tkExpression);

        ExpressionTree expressionTree = tkExpression.getMainToken().buildTree(tkExpression, parenthesisSolver);

        //ExpressionTree newExpressionTree = expressionTree.replaceAllKeys();

        /*while (!expressionTree.equals(newExpressionTree)) {
            expressionTree = newExpressionTree;
            newExpressionTree = expressionTree.replaceAllKeys();
        }*/

        for (int i = 0; i < ExpressionSolver.ITERATIONS; i++) {
            expressionTree = expressionTree.replaceAllKeys();
        }

        expressionTree.showTree();

        assertEquals(11, 11, 0.0001);


    }
}
