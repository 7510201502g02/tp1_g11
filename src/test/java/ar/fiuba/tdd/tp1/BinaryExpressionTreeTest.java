package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.tokens.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinaryExpressionTreeTest {

    @Test
    public void showTree() {

        //Prueba dummy para mostrar la funcionalidad del arbol. El assert no evalua nada :)

        String expression = "8+(3+8*(6/4))-6";

        Scanner scanner = new Scanner();

        TokensExpression tkExpression = scanner.scan(new TransferPackage(expression, null));

        ParenthesisSolver parenthesisSolver = new ParenthesisSolver();

        tkExpression = parenthesisSolver.mapParenthesis(tkExpression);

        ExpressionTree expressionTree = tkExpression.getMainToken().buildTree(tkExpression, parenthesisSolver);

        //ExpressionTree newExpressionTree = expressionTree.replaceAllKeys();

        for (int i = 0; i < ExpressionSolver.ITERATIONS; i++) {
            expressionTree = expressionTree.replaceAllKeys();
        }

        expressionTree.showTree();

        assertEquals(11, 11, 0.0001);

    }
}
