package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;


public class CsvPersistorTest {
    private Persistor persistor;
    private Importer importer;
    private WorkspacesManager<Workbook> workbooks;

    @Before
    public void setUp() {
        workbooks = new WorkspacesManager<>(new Workbook());
        workbooks.create("persistorTest");
        workbooks.get("persistorTest").createNewSheet("first");
        Sheet first = workbooks.get("persistorTest").getSheet("first");
        first.setCellValue("S12", "esto");
        first.setCellValue("Q5", "persisitira");
        first.setCellValue("V7", "por");
        first.setCellValue("A4", "siempre");
    }

    @Test
    public void persistAndImportSimpleSheet() {
        persistor = new Persistor(new CsvPersistence(), workbooks.get("persistorTest"));
        persistor.persistSheet("first", "first.csv");
        workbooks.get("persistorTest").removeSheet("first");
        importer = new Importer(new CsvImport(), workbooks);
        importer.importFile("first.csv", "persistorTest", "first");
        Sheet importedSheet = workbooks.get("persistorTest").getSheet("first");
        assertEquals("first", importedSheet.getName());
        assertEquals("persisitira", importedSheet.getCell("Q5").valueAsString());
        assertEquals("esto", importedSheet.getCell("S12").valueAsString());
        assertEquals("por", importedSheet.getCell("V7").valueAsString());
        assertEquals("siempre", importedSheet.getCell("A4").valueAsString());

        // File cleanup
        File file2 = new File("first.csv");
        file2.delete();

    }
}
