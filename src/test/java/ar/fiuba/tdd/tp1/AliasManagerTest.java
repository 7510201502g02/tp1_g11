package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.BadRangeException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by Leandro on 08-Nov-15. :)
 */
public class AliasManagerTest extends TestCase {

    private AliasManager aliasManager = AliasManager.getInstance();


    @Test
    public void testSaveRangeAlias() throws BadRangeException {
        aliasManager.saveRangeAlias("A1:A9", "RangeA");

        assertEquals("A1:A9", aliasManager.getElementByAlias("RangeA"));
    }

    @Test(expected = BadRangeException.class)
    public void testSaveBadRangeAlias() throws BadRangeException {
        aliasManager.saveRangeAlias("A1:Afrgzdfrg9", "RangeA");

        assertEquals("A1:A9", aliasManager.getElementByAlias("RangeA"));
    }

}
