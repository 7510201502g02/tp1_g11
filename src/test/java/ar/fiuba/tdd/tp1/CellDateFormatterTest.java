package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellDateFormatterTest {

    CellInterface cell;
    CellFormatter cellDateFormatter;

    @Test
    public void createCellDateFormatterWithReverseFormatTest() {

        String validFormat = "yyyy-MM-dd";
        cell = new Cell(null, "2015-10-17T00:00:00Z", null);

        assertEquals("2015-10-17T00:00:00Z", cell.valueAsString());

        cellDateFormatter = new CellDateFormatter(cell, validFormat);

        assertEquals("2015-10-17", cellDateFormatter.valueAsString());

    }


    /*
        no estaria pudiendo validar de una forma piola,
        el SimpleDateFormat intenta cualquier cosa
    */

    /*@Test(expected = IllegalArgumentException.class)
    public void createCellDateFormatterWithInvalidFormatTest() {

        cell = new Cell(null , "2015-10-17T00:00:00Z" , null);
        assertEquals("2015-10-17T00:00:00Z" , cell.valueAsString());

        cellDateFormatter = new CellDateFormatter(cell, "yyyy-ss");

        cellDateFormatter.valueAsString();

    }*/

    /*
        EEUU -> EEUu porque el checkStyle: Abbreviation in name must contain no more than '3' capital letters
        DEJATE DE JODER!!!
    */
    @Test
    public void createCellDateFormatterWithEEUuFormatTest() {

        String validFormat = "MM-dd-yyyy";
        cell = new Cell(null, "2015-10-17T00:00:00Z", null);

        assertEquals("2015-10-17T00:00:00Z", cell.valueAsString());

        cellDateFormatter = new CellDateFormatter(cell, validFormat);

        assertEquals("10-17-2015", cellDateFormatter.valueAsString());

    }

    @Test
    public void createCellDateFormatterWithCommonFormatTest() {

        String validFormat = "dd-MM-yyyy";
        cell = new Cell(null, "2015-10-17T00:00:00Z", null);

        assertEquals("2015-10-17T00:00:00Z", cell.valueAsString());

        cellDateFormatter = new CellDateFormatter(cell, validFormat);

        assertEquals("17-10-2015", cellDateFormatter.valueAsString());

    }

    @Test
    public void notApplyCellDateFormatterIfCellIsANumberTest() {

        cell = new Cell(null, "= 3 + 2", null);

        cellDateFormatter = new CellDateFormatter(cell, "dd-MM-yyyy");

        assertEquals("5", cell.valueAsString());
        assertEquals("5", cellDateFormatter.valueAsString());

    }


    @Test
    public void changeCellDateFormatter() {

        cell = new Cell(null, "2015-10-17T00:00:00Z", null);

        cellDateFormatter = new CellDateFormatter(cell, "MM-dd-yyyy");

        assertEquals("10-17-2015", cellDateFormatter.valueAsString());

        CellDateFormatter newCellDateFormatter = new CellDateFormatter(cell, "dd-MM-yyyy");

        assertEquals("17-10-2015", newCellDateFormatter.valueAsString());

    }

}
