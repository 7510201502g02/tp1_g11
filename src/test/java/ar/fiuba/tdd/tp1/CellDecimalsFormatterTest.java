package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellDecimalsFormatterTest {

    CellInterface cell;
    CellFormatter cellNumberFormatter;

    @Test
    public void createCellNumberFormatterWithTwoDecimalsTest() {


        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellNumberFormatter = new CellDecimalsFormatter(cell, "2");

        assertEquals("20.12", cellNumberFormatter.valueAsString());

    }

    @Test
    public void createCellNumberFormatterWithOutDecimalsTest() {


        cell = new Cell(null, "20.123456", null);

        assertEquals("20.123456", cell.valueAsString());

        cellNumberFormatter = new CellDecimalsFormatter(cell, "0");

        assertEquals("20", cellNumberFormatter.valueAsString());

    }

}
