package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PositionTest {


    @Test
    public void createASimplePositionTest() {

        Position position = new Position("A3");

        assertEquals("A", position.getColumn());
        assertEquals("3", position.getRow());
    }

    @Test
    public void createAComplexPositionTest() {

        Position position = new Position("AB33");

        assertEquals("AB", position.getColumn());
        assertEquals("33", position.getRow());
    }


}
