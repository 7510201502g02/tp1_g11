package ar.fiuba.tdd.tp1;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellSymbolFormatterTest {

    CellInterface cell;
    CellFormatter cellDateFormatter;

    @Test
    public void createCellCurrencyFormatterWithPesosFormatTest() {

        cell = new Cell(null, "34", null);

        /*
            Por default la celda sin formatter, da resultados con un decimal de precision
            y redondeo simetrico
        */

        assertEquals("34", cell.valueAsString());

        cellDateFormatter = new CellSymbolFormatter(cell, "$");

        assertEquals("$ 34", cellDateFormatter.valueAsString());
    }

    @Test
    public void createCellCurrencyFormatterWithDollarsFormatTest() {

        cell = new Cell(null, "34.36", null);

        assertEquals("34.36", cell.valueAsString()); // hace redondeo simetrico

        cellDateFormatter = new CellSymbolFormatter(cell, "US$");

        assertEquals("US$ 34.36", cellDateFormatter.valueAsString());
    }

    @Test
    public void applyCellCurrencyFormatterWithDollarsFormatOnCellWithAFormulaTest() {

        cell = new Cell(null, "= 34 / 7", null);

        assertEquals("4.857143", cell.valueAsString());

        cellDateFormatter = new CellSymbolFormatter(cell, "US$");

        assertEquals("US$ 4.857143", cellDateFormatter.valueAsString());
    }
}
