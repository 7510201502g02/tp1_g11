package ar.fiuba.tdd.tp1;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;

public class WorkspacesManagerTest {

    private WorkspacesManager<Workbook> workspacesManager;

    public void setUp() {
        workspacesManager = new WorkspacesManager<>(new Workbook());
    }

    @Test
    public void getElementTest() {

        setUp();
        workspacesManager.create("test");

        assertEquals(workspacesManager.get("test").getName(), "test");
    }

    @Test(expected = NoSuchElementException.class)
    public void elementNotFoundTest() {

        setUp();
        workspacesManager.get("test");
    }

    @Test(expected = NoSuchElementException.class)
    public void elementRemoveTest() {

        setUp();

        workspacesManager.create("test");

        assertEquals(workspacesManager.get("test").getName(), "test");

        workspacesManager.remove("test");
        workspacesManager.get("test");
    }

    @Test
    public void changeElementNameTest() {

        setUp();
        workspacesManager.create("test");

        assertEquals(workspacesManager.get("test").getName(), "test");

        workspacesManager.changeNameFor("newTest", "test");

        assertEquals(workspacesManager.get("newTest").getName(), "newTest");

    }
}
