package ar.fiuba.tdd.tp1.acceptance.driver;


import ar.fiuba.tdd.tp1.*;
import ar.fiuba.tdd.tp1.exceptions.CircularCellsReferencesException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaExpressionException;
import ar.fiuba.tdd.tp1.exceptions.InvalidValueException;
import ar.fiuba.tdd.tp1.persisting.*;

import java.util.List;
import java.util.NoSuchElementException;

public class SpreadSheetTester implements SpreadSheetTestDriver {

    private WorkspacesManager<Workbook> workBooks = new WorkspacesManager<>(new Workbook());
    private RecordsManager recordsManager = new RecordsManager();


    @Override
    public List<String> workBooksNames() {

        return workBooks.names();

    }

    @Override
    public void createNewWorkBookNamed(String name) {

        workBooks.create(name);

    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {

        // suponemos que lo va encontrar!!
        Workbook workbookToOperate = workBooks.get(workbookName);

        Action createNewWorkSheetNamedAction = new CreateWorkSheetAction(workbookToOperate, name, recordsManager);
        createNewWorkSheetNamedAction.execute();
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {

        return workBooks.get(workBookName).sheetsNames();

    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {

        Sheet sheetToOperate = this.getSheet(workBookName, workSheetName);

        Action setCellValueAction = new SetCellValueAction(sheetToOperate, cellId, value, recordsManager);
        setCellValueAction.execute();

    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {

        Sheet sheetToOperate = this.getSheet(workBookName, workSheetName);
        try {

            return sheetToOperate.getCell(cellId).valueAsString();

        } catch (InvalidFormulaExpressionException e) {

            throw new BadFormulaException();

        }

    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {


        Sheet sheetToOperate = this.getSheet(workBookName, workSheetName);
        try {

            return sheetToOperate.getCell(cellId).valueAsDouble();

        } catch (InvalidFormulaExpressionException e) {
            // si viene de una formula invalidad
            throw new BadFormulaException();

        } catch (CircularCellsReferencesException e) {
            // si viene de una formula invalidad por referencia circular
            throw new BadReferenceException();

        } catch (NumberFormatException e) {
            // si viene de un mal castedo, se podra sacar??
            throw new BadFormulaException();

        } catch (InvalidValueException e) {
            // si viene de un value que no tiene valor as double
            throw new BadFormatException();
        }

    }

    @Override
    public void undo() {

        recordsManager.undo();

    }

    @Override
    public void redo() {

        recordsManager.redo();

    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {

        Sheet sheetToOperate = this.getSheet(workBookName, workSheetName);

        formatter = castFormatterIdentifier(formatter);
        format = castFormatConvention(format, formatter);

        sheetToOperate.setFormatTo(cellId, formatter, format);

    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {

        Sheet sheetToOperate = this.getSheet(workBookName, workSheetName);
        sheetToOperate.setValueTypeTo(cellId, castTypeIdentifier(type));

    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        Workbook workbook = workBooks.get(workBookName);
        Persistor persistor = new Persistor(new JsonPersistence(), workbook);
        persistor.persistWorkBook(fileName);
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        Importer importer = new Importer(new JsonImport(), workBooks);
        importer.importFile(fileName, "", "");
    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String path) {
        Workbook workbook = workBooks.get(workBookName);
        Persistor persistor = new Persistor(new CsvPersistence(), workbook);
        persistor.persistSheet(sheetName, path);
    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String path) {
        Importer importer = new Importer(new CsvImport(), workBooks);
        importer.importFile(path, workBookName, sheetName);
    }

    @Override
    public int sheetCountFor(String workBookName) {
        return this.workBooks.get(workBookName).sheetsNames().size();
    }

    // casteos sobre los formatos.
    /*
        decimal -> decimal // CellDecimalsFormatter
        symbol -> symbol // CellSymbolFormatter
        format -> date  // CellDateFormatter
    */

    private String castFormatterIdentifier(String formatter) {

        String castFormatter = formatter;

        if (formatter.equals("format")) {

            castFormatter = "date";

        }

        return castFormatter;
    }

    // no se crean usando YYYY-MM-DD, forma correcta: yyyy-MM-dd
    private String castFormatConvention(String format, String formatter) {

        if (formatter.equals("date")) {

            format = format.replaceAll("D", "d"); // days
            format = format.replaceAll("Y", "y"); // years
        }

        return format;
    }

    // casteos sobre los tipos.
    /*
        Date -> date // DateValue
        Currency -> number // NumberValue (No representa dinero, representa cualquier numero)
        String -> text  // TextValue
        -> formula // FormulaValue (nosotros diferenciamos las formulas)
    */

    private String castTypeIdentifier(String type) {

        if (type.equals("Date")) {

            type = type.replaceAll("D", "d");

        } else if (type.equals("Currency")) {

            type = "number";

        } else if (type.equals("String")) {

            type = "text";

        }

        return type;
    }

    // metodo auxiliar
    private Sheet getSheet(String workBookName, String workSheetName) {

        try {

            return workBooks.get(workBookName).getSheet(workSheetName);

        } catch (NoSuchElementException e) {

            throw new UndeclaredWorkSheetException();
        }
    }
}
