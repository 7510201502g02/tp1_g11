package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTester;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersistenceTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {

        testDriver = new SpreadSheetTester();
    }

    @Test
    public void persistOneWorkBookAndRetrieveIt() {

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "=10");
        testDriver.setCellValue("tecnicas", "default", "A2", "=A1+1");
        testDriver.setCellValue("tecnicas", "default", "A3", "=A2+1");
        testDriver.setCellValue("tecnicas", "default", "A4", "=A3+1");
        testDriver.setCellValue("tecnicas", "default", "A5", "=A4+1");
        testDriver.persistWorkBook("tecnicas", "proy1.jss");
        testDriver.setCellValue("tecnicas", "default", "A1", "");
        testDriver.setCellValue("tecnicas", "default", "A2", "");
        testDriver.setCellValue("tecnicas", "default", "A3", "");
        testDriver.reloadPersistedWorkBook("proy1.jss");

        assertEquals(10 + 1 + 1 + 1 + 1, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    //@Ignore
    @Test
    public void persistCurrentSheetAsCsv() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "Sheet 1");
        testDriver.createNewWorkSheetNamed("tecnicas", "Sheet 2");
        testDriver.setCellValue("tecnicas", "Sheet 1", "A1", "=10");
        testDriver.setCellValue("tecnicas", "Sheet 1", "A2", "=A1+1");
        testDriver.setCellValue("tecnicas", "Sheet 1", "A3", "=A2+1");
        testDriver.setCellValue("tecnicas", "Sheet 2", "A4", "232");
        testDriver.saveAsCSV("tecnicas", "Sheet 1", "sheet1.csv");

        testDriver.loadFromCSV("tecnicas", "default", "sheet1.csv");

        testDriver.setCellValue("tecnicas", "default", "A2", "5");

        assertEquals(3, testDriver.sheetCountFor("tecnicas"));
        assertEquals("10", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
        assertEquals("5", testDriver.getCellValueAsString("tecnicas", "default", "A2"));
        assertEquals("12", testDriver.getCellValueAsString("tecnicas", "default", "A3"));
    }


}