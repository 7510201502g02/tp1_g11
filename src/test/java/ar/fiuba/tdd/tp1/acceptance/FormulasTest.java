package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.AliasManager;
import ar.fiuba.tdd.tp1.acceptance.driver.*;
import ar.fiuba.tdd.tp1.exceptions.BadRangeException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class FormulasTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {

        testDriver = new SpreadSheetTester();
    }

    @Test
    public void sumLiterals() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 0 + 3.5 + -1");

        assertEquals(3.5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void sumLiteralsInDifferentRows() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 + B2");

        assertEquals(1 + 2, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void subtractLiterals() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 - 0 - 3.5 - -1");

        assertEquals(1 - 3.5 - -1, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferences() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "default", "A2", "5");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + 5 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferenceToReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= 2 + A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "3");

        assertEquals(1 + 2 + 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferencesFromOtherSpreadSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + !other.A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "other", "A2", "-1");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + -1 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }


    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumber() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumberRetrieveAsString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "Hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    /*
        como nosotros inferimos el tipo, para una celda con ese valor
        se crea un valueText, el cual si lo evaluas como double tira
        un bad format exception, y no un bad formula!
        Dejamos el test correspondiente abajo.
    */

    @Ignore
    @Test(expected = BadFormulaException.class)
    public void badFormulaWithoutEqualSymbol() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1 + 2");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormatException.class)
    public void badFormulaWithoutEqualSymbolGroup11Style() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1 + 2");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = UndeclaredWorkSheetException.class)
    public void undeclaredWorkSheetInvocation() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.getCellValueAsString("tecnicas", "undeclaredWorkSheet", "A1");
    }


    @Test(expected = BadReferenceException.class)
    public void formulaWithCyclicReferences() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A1");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test
    public void formulaWithChangesInPreviousCells() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "= 8 + A1");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 10");
        assertEquals(8 + 10, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
    }

    @Test
    public void formulaWithAverageRangeCalculations() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= AVERAGE(A1:A4)");
        assertEquals((10 + 80 + 10 + 20) / 4, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithMaxRangeCalculations() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MAX(A1:A4)");
        assertEquals(80, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }


    @Test
    public void formulaWithMinRangeCalculations() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(A1:A4)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithConcatCells() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "La edad es de ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", " años");
        testDriver.setCellValue("tecnicas", "default", "A5", "= CONCAT(A1,A2,A3)");
        assertEquals("La edad es de 80 años", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void formulaWithConcatCellsWithChangesInPreviousCells() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "La edad es de ");
        testDriver.setCellValue("tecnicas", "default", "B2", "80");
        testDriver.setCellValue("tecnicas", "default", "A2", "=B2");
        testDriver.setCellValue("tecnicas", "default", "A3", " años");
        testDriver.setCellValue("tecnicas", "default", "A5", "= CONCAT(A1,A2,A3)");
        testDriver.setCellValue("tecnicas", "default", "B2", "81");
        assertEquals("La edad es de 81 años", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void testPersistenceOfFormulas() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A1", "3");
        testDriver.setCellValue("gonzalo", "default", "A2", "7");
        testDriver.setCellValue("gonzalo", "default", "A3", "=AVERAGE(A1:A2)");
        testDriver.saveAsCSV("gonzalo", "default", "default");

        assertEquals(5, testDriver.getCellValueAsDouble("gonzalo", "default", "A3"), DELTA);
    }

    @Test
    public void testPow() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A1", "2");
        testDriver.setCellValue("gonzalo", "default", "A2", "2");
        testDriver.setCellValue("gonzalo", "default", "A3", "= A1 ^ A2");

        assertEquals(4, testDriver.getCellValueAsDouble("gonzalo", "default", "A3"), DELTA);
    }

    @Test
    public void testPowWithNegativePow() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A1", "2");
        testDriver.setCellValue("gonzalo", "default", "A2", "-1");
        testDriver.setCellValue("gonzalo", "default", "A3", "= A1 ^ A2");

        assertEquals(0.5, testDriver.getCellValueAsDouble("gonzalo", "default", "A3"), DELTA);
    }

    @Test
    public void testPowWithNegativeBase() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A1", "-1");
        testDriver.setCellValue("gonzalo", "default", "A2", "5");
        testDriver.setCellValue("gonzalo", "default", "A3", "= A1 ^ A2");

        assertEquals(-1, testDriver.getCellValueAsDouble("gonzalo", "default", "A3"), DELTA);
    }

    @Test
    public void testSolveLeft() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A1", "hola");
        testDriver.setCellValue("gonzalo", "default", "A2", "2");
        testDriver.setCellValue("gonzalo", "default", "A3", "=LEFT( A1 , A2 )");

        assertEquals("ho", testDriver.getCellValueAsString("gonzalo", "default", "A3"));
    }

    @Test
    public void testSolveLeftEndOutOfRangeReturnsTheWholeExpression() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A1", "hola");
        testDriver.setCellValue("gonzalo", "default", "A2", "22");
        testDriver.setCellValue("gonzalo", "default", "A3", "=LEFT( A1 , A2 )");

        assertEquals("hola", testDriver.getCellValueAsString("gonzalo", "default", "A3"));
    }

    @Test
    public void testSolvePrintf() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A1", "hola");
        testDriver.setCellValue("gonzalo", "default", "A2", "hola!");
        testDriver.setCellValue("gonzalo", "default", "A4", "Hola $0 $1");
        testDriver.setCellValue("gonzalo", "default", "A3", "=PRINTF( A4 ,A1 , A2 )");

        assertEquals("Hola hola hola!", testDriver.getCellValueAsString("gonzalo", "default", "A3"));
    }

    @Test
    public void testSolvePrintfWhitNoExistingArgument() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A3", "=PRINTF( 'hola$0')");
        assertEquals("hola$0", testDriver.getCellValueAsString("gonzalo", "default", "A3"));
    }

    @Test
    public void testSolvePrintfWhitArgumentOutOfRange() {
        testDriver.createNewWorkBookNamed("gonzalo");
        testDriver.setCellValue("gonzalo", "default", "A2", "hola!");
        testDriver.setCellValue("gonzalo", "default", "A3", "=PRINTF( '$9' , A2 )");

        assertEquals("$9", testDriver.getCellValueAsString("gonzalo", "default", "A3"));
    }

    @Test
    public void testMinWithRangeAlias() throws BadRangeException {
        AliasManager aliasManager = AliasManager.getInstance();
        aliasManager.saveRangeAlias("A1:A4", "RangeA");
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(RangeA)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);

    }

    @Test
    public void testMaxWithRangeAlias() throws BadRangeException {
        AliasManager aliasManager = AliasManager.getInstance();
        aliasManager.saveRangeAlias("A1:A4", "RangeA");
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MAX(RangeA)");
        assertEquals(80, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);

    }

    @Test
    public void testAverageWithRangeAlias() throws BadRangeException {
        AliasManager aliasManager = AliasManager.getInstance();
        aliasManager.saveRangeAlias("A1:A4", "RangeA");
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= AVERAGE(RangeA)");
        assertEquals((10 + 80 + 10 + 20) / 4, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);

    }

    @Test
    public void testSolveToday() {
        testDriver.createNewWorkBookNamed("tecnicas");
        String dateAssigned = LocalDateTime.now().toString();
        testDriver.setCellValue("tecnicas", "default", "A5", "= TODAY()");
        String cellValue = testDriver.getCellValueAsString("tecnicas", "default", "A5");
        assertEquals(dateAssigned.substring(0, dateAssigned.length() - 3), cellValue.substring(0, cellValue.length() - 3));
    }
}
