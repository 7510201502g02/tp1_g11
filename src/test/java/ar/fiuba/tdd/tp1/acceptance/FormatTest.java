package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormatException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTester;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FormatTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {

        testDriver = new SpreadSheetTester();

    }

    /*
        En nuestro disenio no existen los tipos, lo mas parecido
        se llama Value que es lo que puede contener la clase Cell,
        Existen distintos "tipos" de Value: Text, Formula, Date, Number.
        Cada Value se infiere por el sistema, es decir, si vos
        en la celda pones un 2 la celda le pide al ValueCreator
        que le devuelva el objeto Value correspondiente (En este
        caso Number). A su vez, si le agregas algun CellFormatter,
        el mismo se aplica si corresponde sino deja el formato preexistente.

        + Agregamos la posibilidad de forzar el valueType para poder cumplir
        mejor los test propuestos. Es decir se puede forzar al ValueCreator
        a poner "crear" un value, aunque sea incorrecto. En este ultimo caso,
        el ValueCreator retorna un InvalidValue, el cual maneja el "flujo"
        de excepciones.

    */

    @Test(expected = BadFormatException.class)
    public void stringIsNotNumber() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "String");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test
    public void formulaAsString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 2");
        testDriver.setCellType("tecnicas", "default", "A1", "String");
        assertEquals("= 1 + 2", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatEuropean() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "DD-MM-YYYY");
        assertEquals("14-07-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatAmerican() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "MM-DD-YYYY");
        assertEquals("07-14-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatISO8601() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "YYYY-MM-DD");
        assertEquals("2012-07-14", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateNoAutoformat() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        assertEquals("2012-07-14T00:00:00Z", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void badDateFormat() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Not a date");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "YYYY-MM-DD");
        assertEquals("Error:BAD_DATE", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    /*
        para salvar el test estamos castiando Currency a number,
        que es un tipo existente en nuestro sistema.
    */

    @Test
    public void moneyFormatUSD() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        assertEquals("U$S 2", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    /*
        para salvar el test estamos castiando Currency a number,
        que es un tipo existente en nuestro sistema.
    */

    @Test
    public void moneyFormatUSDAndDecimal() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "decimal", "2");
        assertEquals("U$S 2.00", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    /*
        Nosotros no tenemos un type currency, pero si un number.
        Debajo de este test dejamos el equivalente.
    */

    @Ignore
    @Test
    public void badMoneyFormat() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Not money");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        assertEquals("Error:BAD_CURRENCY", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void badMoneyFormatGroup11Style() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Not money");
        testDriver.setCellType("tecnicas", "default", "A1", "number"); // no sirve de nada!
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        assertEquals("Error:BAD_NUMBER", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
}